package log

import (
	"sync/atomic"
)

type AtomicLogger struct {
	p atomic.Pointer[Logger]
}

func NewAtomic(logger Logger) *AtomicLogger {
	l := &AtomicLogger{}
	l.Set(logger)
	return l
}

func (l *AtomicLogger) IsNop() bool {
	lp := l.p.Load()
	return lp == nil || (*lp).IsNop()
}

func (l *AtomicLogger) Logger() Logger {
	lp := l.p.Load()
	if lp != nil {
		return *lp
	}
	return Nop
}

func (l *AtomicLogger) Set(logger Logger) {
	l.p.Store(&logger)
}

func (l *AtomicLogger) Swap(logger Logger) Logger {
	lp := l.p.Swap(&logger)
	if lp == nil {
		return Nop
	}
	return *lp
}

func (l *AtomicLogger) Modify(fn func(logger Logger) Logger) {
	for {
		lp := l.p.Load()
		if lp == nil {
			return
		}
		nl := fn(*lp)
		if l.p.CompareAndSwap(lp, &nl) {
			return
		}
	}
}

func (l *AtomicLogger) Log(level LevelI, msg string, fields ...Field) {
	l.Logger().AddCallerSkip(1).Log(level, msg, fields...)
}

func (l *AtomicLogger) Trace(msg string, fields ...Field) {
	l.Logger().AddCallerSkip(1).Trace(msg, fields...)
}

func (l *AtomicLogger) Debug(msg string, fields ...Field) {
	l.Logger().AddCallerSkip(1).Debug(msg, fields...)
}

func (l *AtomicLogger) Info(msg string, fields ...Field) {
	l.Logger().AddCallerSkip(1).Info(msg, fields...)
}

func (l *AtomicLogger) Warn(msg string, fields ...Field) {
	l.Logger().AddCallerSkip(1).Warn(msg, fields...)
}

func (l *AtomicLogger) Error(msg string, fields ...Field) {
	l.Logger().AddCallerSkip(1).Error(msg, fields...)
}

func (l *AtomicLogger) DPanic(msg string, fields ...Field) {
	l.Logger().AddCallerSkip(1).DPanic(msg, fields...)
}

func (l *AtomicLogger) Panic(msg string, fields ...Field) {
	l.Logger().AddCallerSkip(1).Panic(msg, fields...)
}

func (l *AtomicLogger) Fatal(msg string, fields ...Field) {
	l.Logger().AddCallerSkip(1).Fatal(msg, fields...)
}

func (l *AtomicLogger) Logf(level LevelI, format string, args ...any) {
	l.Logger().AddCallerSkip(1).Logf(level, format, args...)
}

func (l *AtomicLogger) Tracef(format string, args ...any) {
	l.Logger().AddCallerSkip(1).Tracef(format, args...)
}

func (l *AtomicLogger) Debugf(format string, args ...any) {
	l.Logger().AddCallerSkip(1).Debugf(format, args...)
}

func (l *AtomicLogger) Infof(format string, args ...any) {
	l.Logger().AddCallerSkip(1).Infof(format, args...)
}

func (l *AtomicLogger) Warnf(format string, args ...any) {
	l.Logger().AddCallerSkip(1).Warnf(format, args...)
}

func (l *AtomicLogger) Errorf(format string, args ...any) {
	l.Logger().AddCallerSkip(1).Errorf(format, args...)
}

func (l *AtomicLogger) DPanicf(format string, args ...any) {
	l.Logger().AddCallerSkip(1).DPanicf(format, args...)
}

func (l *AtomicLogger) Panicf(format string, args ...any) {
	l.Logger().AddCallerSkip(1).Panicf(format, args...)
}

func (l *AtomicLogger) Fatalf(format string, args ...any) {
	l.Logger().AddCallerSkip(1).Fatalf(format, args...)
}
