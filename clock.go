package log

import "time"

// DefaultClock 默认的时钟使用操作系统提供的时钟
var DefaultClock = SystemClock{}

// Clock is a source of time for logged entries.
type Clock interface {
	// Now returns the current local time.
	Now() time.Time

	// NewTicker returns *time.Ticker that holds a channel
	// that delivers "ticks" of a clock.
	NewTicker(time.Duration) *time.Ticker
}

// SystemClock implements default Clock that uses system time.
type SystemClock struct{}

func (SystemClock) Now() time.Time {
	return time.Now()
}

func (SystemClock) NewTicker(duration time.Duration) *time.Ticker {
	return time.NewTicker(duration)
}
