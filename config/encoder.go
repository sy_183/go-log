package config

import (
	"gitee.com/csstx2020/cvds2020/go-common/container"
	"gitee.com/csstx2020/cvds2020/go-common/yaml"
	"gitee.com/csstx2020/cvds2020/go-log"
)

type EncoderProvider func(options ...log.EncoderOption) log.Encoder

type EncoderType struct {
	Name     string
	Provider EncoderProvider
}

const TargetEncoderType = "encoder type"

func (tp *EncoderType) UnmarshalText(text []byte) error {
	if tp == nil {
		return log.NewUnmarshalNilError(tp)
	}
	typ := string(text)
	t, ok := GetEncoderType(typ)
	if !ok {
		return log.NewUnknownError(TargetEncoderType, typ)
	}
	*tp = t
	return nil
}

var encoderTypes container.SyncMap[string, EncoderType]

func RegisterEncoder(typ EncoderType) bool {
	_, exist := encoderTypes.LoadOrStore(typ.Name, typ)
	return !exist
}

func GetEncoderType(name string) (EncoderType, bool) {
	return encoderTypes.Load(name)
}

func init() {
	RegisterEncoder(EncoderType{Name: "json", Provider: log.NewJsonEncoder})
	RegisterEncoder(EncoderType{Name: "console", Provider: log.NewConsoleEncoder})
}

type (
	TimeFormatter       struct{ log.TimeFormatter }
	LayoutTimeFormatter struct {
		Layout string `yaml:"layout"`
	}
)

func (f *TimeFormatter) UnmarshalYAMLScalar(value *yaml.Node) error {
	if f == nil {
		return log.NewUnmarshalNilError(f)
	}
	return f.UnmarshalText([]byte(value.Value))
}

func (f *TimeFormatter) UnmarshalYAMLMapping(value *yaml.Node) error {
	if f == nil {
		return log.NewUnmarshalNilError(f)
	}
	tf := LayoutTimeFormatter{}
	if err := value.Decode(&tf); err != nil {
		return err
	}
	*f = TimeFormatter{log.TimeFormatterOfLayout(tf.Layout)}
	return nil
}

func WithTimeFormatter(formatter TimeFormatter) log.EncoderOption {
	return log.WithTimeFormatter(formatter.TimeFormatter)
}

type EncoderConfigType = OptionType[log.EncoderOption]

var encoderConfigTypes container.SyncMap[string, EncoderConfigType]

func init() {
	RegisterEncoderConfig("message-key", ZeroOptionType(log.WithMessageKey, nil))
	RegisterEncoderConfig("level-key", ZeroOptionType(log.WithLevelKey, nil))
	RegisterEncoderConfig("time-key", ZeroOptionType(log.WithTimeKey, nil))
	RegisterEncoderConfig("name-key", ZeroOptionType(log.WithNameKey, nil))
	RegisterEncoderConfig("caller-key", ZeroOptionType(log.WithCallerKey, nil))
	RegisterEncoderConfig("function-key", ZeroOptionType(log.WithFunctionKey, nil))
	RegisterEncoderConfig("stacktrace-key", ZeroOptionType(log.WithStacktraceKey, nil))
	RegisterEncoderConfig("array-start-space", ZeroOptionType(log.WithArrayStartSpace, nil))
	RegisterEncoderConfig("array-end-space", ZeroOptionType(log.WithArrayEndSpace, nil))
	RegisterEncoderConfig("array-space", ZeroOptionType(log.WithArraySpace, nil))
	RegisterEncoderConfig("map-start-space", ZeroOptionType(log.WithMapStartSpace, nil))
	RegisterEncoderConfig("map-end-space", ZeroOptionType(log.WithMapEndSpace, nil))
	RegisterEncoderConfig("map-space", ZeroOptionType(log.WithMapSpace, nil))
	RegisterEncoderConfig("namespace-space", ZeroOptionType(log.WithNamespaceSpace, nil))
	RegisterEncoderConfig("key-value-space", ZeroOptionType(log.WithKeyValueSpace, nil))
	RegisterEncoderConfig("elem-space", ZeroOptionType(log.WithElemSpace, nil))
	RegisterEncoderConfig("skip-line-ending", ZeroOptionType(log.WithSkipLineEnding, nil))
	RegisterEncoderConfig("line-ending", ZeroOptionType(log.WithLineEnding, log.CheckLineEnding))
	RegisterEncoderConfig("indent", ZeroOptionType(log.WithIndent, log.CheckIndent))
	RegisterEncoderConfig("indent-prefix", ZeroOptionType(log.WithIndentPrefix, log.CheckIndentPrefix))
	RegisterEncoderConfig("escape-html", ZeroOptionType(log.WithEscapeHTML, nil))
	RegisterEncoderConfig("escape-esc", ZeroOptionType(log.WithEscapeESC, nil))
	RegisterEncoderConfig("level-formatter", ZeroOptionType(log.WithLevelFormatter, nil))
	RegisterEncoderConfig("time-formatter", ZeroOptionType(WithTimeFormatter, nil))
	RegisterEncoderConfig("duration-formatter", ZeroOptionType(log.WithDurationFormatter, nil))
	RegisterEncoderConfig("caller-formatter", ZeroOptionType(log.WithCallerFormatter, nil))
	RegisterEncoderConfig("name-formatter", ZeroOptionType(log.WithNameFormatter, nil))
	RegisterEncoderConfig("encode-message", ZeroOptionType(log.WithEncodeMessage, nil))
	RegisterEncoderConfig("encode-level", ZeroOptionType(log.WithEncodeLevel, nil))
	RegisterEncoderConfig("encode-time", ZeroOptionType(log.WithEncodeTime, nil))
	RegisterEncoderConfig("encode-name", ZeroOptionType(log.WithEncodeName, nil))
	RegisterEncoderConfig("encode-caller", ZeroOptionType(log.WithEncodeCaller, nil))
	RegisterEncoderConfig("encode-function", ZeroOptionType(log.WithEncodeFunction, nil))
	RegisterEncoderConfig("encode-stacktrace", ZeroOptionType(log.WithEncodeStacktrace, nil))
	RegisterEncoderConfig("console-separator", ZeroOptionType(log.WithConsoleSeparator, nil))
	RegisterEncoderConfig("json-indent-new-line", ZeroOptionType(log.WithJsonIndentNewLine, nil))
}

func RegisterEncoderConfig(name string, typ EncoderConfigType) bool {
	_, exist := encoderConfigTypes.LoadOrStore(name, typ)
	return !exist
}

func GetEncoderConfig(name string) EncoderConfigType {
	typ, _ := encoderConfigTypes.Load(name)
	return typ
}

type EncoderOptionMapper struct{}

func (EncoderOptionMapper) OptionType(name string) EncoderConfigType {
	return GetEncoderConfig(name)
}

type Encoder struct {
	Type    EncoderType                                     `yaml:"type"`
	Options Options[log.EncoderOption, EncoderOptionMapper] `yaml:",inline"`
}
