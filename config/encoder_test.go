package config

import (
	"bufio"
	"fmt"
	"gitee.com/csstx2020/cvds2020/go-common/assert"
	"gitee.com/csstx2020/cvds2020/go-common/yaml"
	"os"
	"testing"
)

func TestEncoder(t *testing.T) {
	cfg := Encoder{}
	fp := assert.Must(os.Open("encoder.yaml"))
	defer fp.Close()
	assert.MustSuccess(yaml.NewDecoder(bufio.NewReader(fp)).Decode(&cfg))
	fmt.Println(cfg)
}
