package config

import (
	"gitee.com/csstx2020/cvds2020/go-common/container"
	"gitee.com/csstx2020/cvds2020/go-common/yaml"
	"gitee.com/csstx2020/cvds2020/go-log"
	"reflect"
)

type OptionType[OPT any] interface {
	Zero() any
	Check(value any) error
	Option(value any) OPT
}

type zeroOptionType[V any, OPT any] struct {
	option  func(value V) OPT
	checker func(value V) error
}

func ZeroOptionType[V any, OPT any](option func(value V) OPT, checker func(value V) error) OptionType[OPT] {
	return zeroOptionType[V, OPT]{
		option:  option,
		checker: checker,
	}
}

func (t zeroOptionType[V, OPT]) Zero() any {
	var zero V
	return zero
}

func (t zeroOptionType[V, OPT]) Check(value any) error {
	if t.checker != nil {
		return t.checker(value.(V))
	}
	return nil
}

func (t zeroOptionType[V, OPT]) Option(value any) OPT {
	return t.option(value.(V))
}

type OptionMapper[OPT any] interface {
	OptionType(name string) OptionType[OPT]
}

type OptionEntry[OPT any] struct {
	Type  OptionType[OPT]
	Value any
}

type Options[OPT any, MAPPER OptionMapper[OPT]] struct {
	container.LinkedMap[string, OptionEntry[OPT]]
}

func reflectNewPointer(v any) reflect.Value {
	vt, vv := reflect.TypeOf(v), reflect.ValueOf(v)
	npv := reflect.New(vt)
	npv.Elem().Set(vv)
	return npv
}

func (o *Options[OPT, MAPPER]) UnmarshalYAMLEntry(key *yaml.Node, value *yaml.Node) (ok bool, err error) {
	if o == nil {
		return false, log.NewUnmarshalNilError(o)
	}
	var mapper MAPPER
	if typ := mapper.OptionType(key.Value); typ != nil {
		zp := reflectNewPointer(typ.Zero())
		if err = value.Decode(zp.Interface()); err != nil {
			return true, err
		}
		v := zp.Elem().Interface()
		if err = typ.Check(v); err != nil {
			return true, err
		}
		if o.IsNil() {
			o.Init(0, 0)
		}
		o.Store(key.Value, OptionEntry[OPT]{Type: typ, Value: v})
	}
	return false, nil
}
