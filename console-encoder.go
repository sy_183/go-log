package log

import (
	"gitee.com/csstx2020/cvds2020/go-common/unsafe"
	"gitee.com/csstx2020/cvds2020/go-log/internal/bufferpool"
	"time"
)

type (
	consoleEncoderConfig struct {
		encodeMessage     bool
		encodeLevel       bool
		encodeTime        bool
		encodeName        bool
		encodeCaller      bool
		encodeFunction    bool
		encodeStacktrace  bool
		consoleSeparator  string
		jsonIndentNewLine bool
	}
	ceCfg = consoleEncoderConfig
)

func (c *ceCfg) setEncodeMessage(enable bool)     { c.encodeMessage = enable }
func (c *ceCfg) setEncodeLevel(enable bool)       { c.encodeLevel = enable }
func (c *ceCfg) setEncodeTime(enable bool)        { c.encodeTime = enable }
func (c *ceCfg) setEncodeName(enable bool)        { c.encodeName = enable }
func (c *ceCfg) setEncodeCaller(enable bool)      { c.encodeCaller = enable }
func (c *ceCfg) setEncodeFunction(enable bool)    { c.encodeFunction = enable }
func (c *ceCfg) setEncodeStacktrace(enable bool)  { c.encodeStacktrace = enable }
func (c *ceCfg) setConsoleSeparator(sep string)   { c.consoleSeparator = sep }
func (c *ceCfg) setJsonIndentNewLine(enable bool) { c.jsonIndentNewLine = enable }

type consoleEncoderSeparator int

const (
	consoleEncoderSeparatorStart consoleEncoderSeparator = iota
	consoleEncoderSeparatorElement
	consoleEncoderSeparatorAdded
)

type (
	consoleEncoderCtx struct {
		jsonEncoderCtx
		*consoleEncoderConfig
		sep consoleEncoderSeparator
	}
	cec = consoleEncoderCtx
)

func (c *cec) encodeNil() error {
	encodeNil(c.buffer)
	return nil
}

func (c *cec) encodeBinary(val []byte) error {
	return encodeBinary(c.buffer, val)
}

func (c *cec) encodeBool(val bool) error {
	encodeBool(c.buffer, val)
	return nil
}

func (c *cec) encodeComplex(val complex128, precision int) error {
	encodeComplex(c.buffer, val, precision)
	return nil
}

func (c *cec) encodeComplex128(val complex128) error { return c.encodeComplex(val, 64) }
func (c *cec) encodeComplex64(val complex64) error   { return c.encodeComplex(complex128(val), 32) }

func (c *cec) encodeFloat(val float64, bitSize int) error {
	c.buffer.AppendFloat(val, bitSize)
	return nil
}

func (c *cec) encodeFloat64(val float64) error { return c.encodeFloat(val, 64) }
func (c *cec) encodeFloat32(val float32) error { return c.encodeFloat(float64(val), 32) }

func (c *cec) encodeString(s string) error {
	c.buffer.AppendString(s)
	return nil
}

func (c *cec) encodeTime(val time.Time) error         { return c.encodeValue(c.FormatTime(val)) }
func (c *cec) encodeDuration(val time.Duration) error { return c.encodeValue(c.FormatDuration(val)) }

func (c *cec) encodeNilV(value Value) error        { return c.encodeNil() }
func (c *cec) encodeBinaryV(value Value) error     { return c.encodeBinary(value.Binary()) }
func (c *cec) encodeBoolV(value Value) error       { return c.encodeBool(value.Bool()) }
func (c *cec) encodeComplex128V(value Value) error { return c.encodeComplex128(value.Complex128()) }
func (c *cec) encodeComplex64V(value Value) error  { return c.encodeComplex64(value.Complex64()) }
func (c *cec) encodeFloat64V(value Value) error    { return c.encodeFloat64(value.Float64()) }
func (c *cec) encodeFloat32V(value Value) error    { return c.encodeFloat32(value.Float32()) }
func (c *cec) encodeStringV(value Value) error     { return c.encodeString(value.String()) }
func (c *cec) encodeTimeV(value Value) error       { return c.encodeTime(value.Time()) }
func (c *cec) encodeTimeFullV(value Value) error   { return c.encodeTime(value.TimeFull()) }
func (c *cec) encodeDurationV(value Value) error   { return c.encodeDuration(value.Duration()) }
func (c *cec) encodeStringerV(value Value) error   { return c.encodeString(value.Stringer().String()) }

func (c *cec) encodeValue(value Value) error {
	if value.Type >= MaxTypes {
		return InvalidValueTypeError{Type: value.Type}
	}
	if encoder := consoleEncoderMap[value.Type]; encoder != nil {
		return encoder(c, value)
	}
	return InvalidValueTypeError{Type: value.Type}
}

func (c *cec) FormatLevel(l LevelI) Value {
	formatter := c.levelFormatter
	if formatter == nil {
		formatter = CapitalLevelFormatter
	}
	return formatter(l)
}

func (c *cec) FormatTime(t time.Time) Value {
	formatter := c.timeFormatter
	if formatter == nil {
		formatter = DefaultTimeFormatter
	}
	return formatter(t)
}

func (c *cec) FormatDuration(duration time.Duration) Value {
	formatter := c.durationFormatter
	if formatter == nil {
		formatter = SecondDurationFormatter
	}
	return formatter(duration)
}

func (c *cec) FormatCaller(caller EntryCaller) Value {
	formatter := c.callerFormatter
	if formatter == nil {
		formatter = FullCallerFormatter
	}
	return formatter(caller)
}

var consoleEncoderMap [MaxTypes]func(*cec, Value) error

func init() {
	consoleEncoderMap = [MaxTypes]func(*cec, Value) error{
		SkipType:       (*cec).encodeSkipV,
		NilType:        (*cec).encodeNilV,
		BinaryType:     (*cec).encodeBinaryV,
		BoolType:       (*cec).encodeBoolV,
		Complex128Type: (*cec).encodeComplex128V,
		Complex64Type:  (*cec).encodeComplex64V,
		Float64Type:    (*cec).encodeFloat64V,
		Float32Type:    (*cec).encodeFloat32V,
		Int64Type:      (*cec).encodeInt64V,
		Int32Type:      (*cec).encodeInt32V,
		Int16Type:      (*cec).encodeInt16V,
		Int8Type:       (*cec).encodeInt8V,
		StringType:     (*cec).encodeStringV,
		Uint64Type:     (*cec).encodeUint64V,
		Uint32Type:     (*cec).encodeUint32V,
		Uint16Type:     (*cec).encodeUint16V,
		Uint8Type:      (*cec).encodeUint8V,
		UintptrType:    (*cec).encodeUintptrV,
		TimeType:       (*cec).encodeTimeV,
		TimeFullType:   (*cec).encodeTimeFullV,
		DurationType:   (*cec).encodeDurationV,
		StringerType:   (*cec).encodeStringerV,
		FormatterType:  (*cec).encodeFormatterV,
		ErrorType:      (*cec).encodeErrorV,
		ObjectType:     (*cec).encodeObjectV,
		SliceType:      (*cec).encodeObjectV,
		MapType:        (*cec).encodeObjectV,
		ValuesType:     (*cec).encodeValuesV,
		FieldsType:     (*cec).encodeFieldsV,
	}
}

func (c *cec) addSeparator() {
	if c.sep == consoleEncoderSeparatorElement {
		c.buffer.AppendString(c.consoleSeparator)
	}
	c.sep = consoleEncoderSeparatorAdded
	c.jsonEncoderCtx.sep = jsonEncoderSeparatorAdded
}

func (c *cec) appendValue(value Value) error {
	c.addSeparator()
	if err := c.encodeValue(value); err != nil {
		return err
	}
	c.sep = consoleEncoderSeparatorElement
	return nil
}

func (c *cec) appendString(s string) {
	c.addSeparator()
	c.buffer.WriteString(s)
	c.sep = consoleEncoderSeparatorElement
}

func (c *cec) encode(entry *Entry, fields []Field) (res any, err error) {
	if c.consoleEncoderConfig.encodeTime {
		if err = c.appendValue(c.FormatTime(entry.Time)); err != nil {
			return nil, err
		}
	}

	if c.encodeLevel {
		if err = c.appendValue(c.FormatLevel(entry.Level)); err != nil {
			return nil, err
		}
	}

	if c.encodeName && entry.LoggerName != "" {
		if err = c.appendValue(c.FormatName(entry.LoggerName)); err != nil {
			return nil, err
		}
	}

	if entry.Caller.Defined {
		if c.encodeCaller {
			if err := c.appendValue(c.FormatCaller(entry.Caller)); err != nil {
				return nil, err
			}
		}
		if c.encodeFunction {
			c.appendString(entry.Caller.Function)
		}
	}

	if c.encodeMessage && entry.Message != "" {
		c.buffer.AppendByte(':')
		c.appendString(entry.Message)
	}

	var fn int
	for i := range fields {
		if fields[i].Type != SkipType {
			fn++
		}
	}
	for i := range entry.Fields {
		if entry.Fields[i].Type != SkipType {
			fn++
		}
	}
	if fn > 0 {
		if len(c.indent) > 0 && c.jsonIndentNewLine {
			c.buffer.AppendString(c.lineEnding)
			c.jsonEncoderCtx.sep = jsonEncoderSeparatorDocumentStart
		} else {
			c.addSeparator()
			c.jsonEncoderCtx.sep = jsonEncoderSeparatorAdded
		}
		var n1, n2 int
		c.jsonEncoderCtx.openMap()
		n1, err = c.jsonEncoderCtx.encodeFieldsElements(entry.Fields)
		if err != nil {
			return nil, err
		}
		n2, err = c.jsonEncoderCtx.encodeFieldsElements(fields)
		if err != nil {
			return nil, err
		}
		c.jsonEncoderCtx.closeMap(n1+n2 == 0)
	}

	if c.encodeStacktrace && entry.Stack != "" {
		c.buffer.AppendString(c.lineEnding)
		c.buffer.AppendString(entry.Stack)
	}

	if !c.skipLineEnding {
		c.buffer.AppendString(c.lineEnding)
	}

	return c.buffer, nil
}

type consoleEncoder struct {
	jsonEncoder
	*consoleEncoderConfig
}

func NewConsoleEncoder(options ...EncoderOption) Encoder {
	enc := consoleEncoder{
		jsonEncoder: jsonEncoder{
			jsonEncoderConfig: &jsonEncoderConfig{
				keyValueSpace:     1,
				elemSpace:         1,
				lineEnding:        DefaultLineEnding.Get(),
				escapeHTML:        false,
				levelFormatter:    CapitalLevelFormatter,
				timeFormatter:     DefaultTimeFormatter,
				durationFormatter: StringDurationFormatter,
				callerFormatter:   ShortCallerFormatter,
				errorHandler:      DefaultEncoderErrorHandler,
			},
		},
		consoleEncoderConfig: &consoleEncoderConfig{
			encodeMessage:     true,
			encodeLevel:       true,
			encodeTime:        true,
			encodeName:        true,
			encodeCaller:      true,
			encodeFunction:    true,
			encodeStacktrace:  true,
			consoleSeparator:  "\t",
			jsonIndentNewLine: true,
		},
	}
	for _, option := range options {
		option.Apply(enc)
	}
	return enc
}

func (enc consoleEncoder) clone() consoleEncoder {
	return consoleEncoder{
		jsonEncoder:          enc.jsonEncoder.clone(),
		consoleEncoderConfig: clone(enc.consoleEncoderConfig),
	}
}

func (enc consoleEncoder) WithOptions(options ...EncoderOption) Encoder {
	n := enc.clone()
	for _, option := range options {
		option.Apply(n)
	}
	return n
}

func (enc consoleEncoder) Encode(logger Logger, entry *Entry, fields ...Field) (any, error) {
	ctx := unsafe.Unescape(&consoleEncoderCtx{
		jsonEncoderCtx: jsonEncoderCtx{
			jsonEncoderConfig: enc.jsonEncoderConfig,
			buffer:            bufferpool.Get(),
		},
		consoleEncoderConfig: enc.consoleEncoderConfig,
	})
	res, err := ctx.encode(entry, fields)
	if err != nil {
		if enc.errorHandler != nil {
			enc.errorHandler(enc, logger, err)
		}
		ctx.buffer.Free()
		return nil, err
	}
	return res, nil
}
