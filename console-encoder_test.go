package log

import (
	"errors"
	"fmt"
	"gitee.com/csstx2020/cvds2020/go-log/internal/bufferpool"
	"os"
	"testing"
	"time"
)

func TestConsoleEncoder_Encode(t *testing.T) {
	enc := NewConsoleEncoder(
		WithLevelFormatter(CapitalColorLevelFormatter),
		//WithNamespaceSpace(1),
		WithIndent("  "),
		WithIndentPrefix("  "),
	)
	buffer, err := enc.Encode(Nop, &Entry{
		Level:      InfoLevel,
		Time:       time.Now(),
		LoggerName: "test",
		Message:    "test console encoder",
	}, String("key1", "string value 1"),
		Values("values1", StringV("value1"), IntV(0), Float64V(3.14)),
		Fields("fields1",
			String("key1", "string value 1"),
			Complex128("key2", 4+3i),
			Duration("key3", time.Second),
		),
	)
	if err != nil {
		panic(err)
	}
	os.Stdout.Write(buffer.(*bufferpool.Buffer).Bytes())
}

func TestFormatError(t *testing.T) {
	fmt.Printf("%s\n", errors.New("123"))
}
