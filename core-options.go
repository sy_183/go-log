package log

import (
	flagPkg "gitee.com/csstx2020/cvds2020/go-common/flag"
	"gitee.com/csstx2020/cvds2020/go-common/option"
	"os"
)

type CoreOption = option.Setter[Core]

func WithSink(sink Sink) CoreOption {
	type SinkSetter interface{ setSink(sink Sink) }
	return option.Set[Core](func(target Core) {
		if setter, is := target.(SinkSetter); is {
			setter.setSink(sink)
		}
	})
}

var (
	WithNopSink = WithSink(NopSink)
	WithStdout  = WithSink(Stdout)
	WithStderr  = WithSink(Stderr)
)

func WithCoreErrorHandler(handler CoreErrorHandler) CoreOption {
	type CoreErrorHandlerSetter interface {
		setErrorHandler(handler CoreErrorHandler)
	}
	return option.Set[Core](func(target Core) {
		if setter, is := target.(CoreErrorHandlerSetter); is {
			setter.setErrorHandler(handler)
		}
	})
}

func WithQueueSize(size int) CoreOption {
	type QueueSizeSetter interface{ setQueueSize(size int) }
	return option.Set[Core](func(target Core) {
		if setter, is := target.(QueueSizeSetter); is {
			setter.setQueueSize(size)
		}
	})
}

func WithBufferSize(size int) CoreOption {
	type BufferSizeSetter interface{ setBufferSize(size int) }
	return option.Set[Core](func(target Core) {
		if setter, is := target.(BufferSizeSetter); is {
			setter.setBufferSize(size)
		}
	})
}

func WithAsyncDropHandler(handler AsyncDropHandler) CoreOption {
	type AsyncDropHandlerSetter interface {
		setDropHandler(handler AsyncDropHandler)
	}
	return option.Set[Core](func(target Core) {
		if setter, is := target.(AsyncDropHandlerSetter); is {
			setter.setDropHandler(handler)
		}
	})
}

func WithFilePath(name string) CoreOption {
	type FileNameSetter interface{ setPath(name string) }
	return option.Set[Core](func(target Core) {
		if setter, is := target.(FileNameSetter); is {
			setter.setPath(name)
		}
	})
}

func WithFileFlag(flag int) CoreOption {
	flag = (flag & os.O_SYNC) | (flag & os.O_TRUNC) | os.O_CREATE | os.O_WRONLY
	if !flagPkg.TestFlag(flag, os.O_TRUNC) {
		flag |= os.O_APPEND
	}
	type FileFlagSetter interface{ setFlag(flag int) }
	return option.Set[Core](func(target Core) {
		if setter, is := target.(FileFlagSetter); is {
			setter.setFlag(flag)
		}
	})
}

func WithFileMode(mode os.FileMode) CoreOption {
	type FileModeSetter interface{ setMode(mode os.FileMode) }
	return option.Set[Core](func(target Core) {
		if setter, is := target.(FileModeSetter); is {
			setter.setMode(mode)
		}
	})
}
