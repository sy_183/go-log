package log

import (
	"io"
	"reflect"
)

type Core interface {
	Open() error
	Write(Logger, *Entry, []Field, any) error
	Sync() error
	io.Closer
}

type nopCore struct{}

func (c nopCore) Open() error                              { return nil }
func (c nopCore) Write(Logger, *Entry, []Field, any) error { return nil }
func (c nopCore) Sync() error                              { return nil }
func (c nopCore) Close() error                             { return nil }

var NopCore = Core(nopCore{})

func ProvideNopCore(options ...CoreOption) Core { return NopCore }

type CoreErrorHandler func(Core, Logger, error)

func DefaultCoreErrorHandler(core Core, logger Logger, err error) {
	if !logger.IsNop() {
		logger.Error("日志输出失败", Stringer("输出器类型", reflect.TypeOf(core)), Err("错误", err))
	}
}
