package log

import (
	"gitee.com/csstx2020/cvds2020/go-common/assert"
	"strings"

	"gitee.com/csstx2020/cvds2020/go-common/option"
)

type EncoderOption = option.Setter[Encoder]

func WithMessageKey(key string) EncoderOption {
	type MessageKeySetter interface{ setMessageKey(key string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(MessageKeySetter); is {
			setter.setMessageKey(key)
		}
	})
}

func WithLevelKey(key string) EncoderOption {
	type LevelKeySetter interface{ setLevelKey(key string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(LevelKeySetter); is {
			setter.setLevelKey(key)
		}
	})
}

func WithTimeKey(key string) EncoderOption {
	type TimeKeySetter interface{ setTimeKey(key string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(TimeKeySetter); is {
			setter.setTimeKey(key)
		}
	})
}

func WithNameKey(key string) EncoderOption {
	type NameKeySetter interface{ setNameKey(key string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(NameKeySetter); is {
			setter.setNameKey(key)
		}
	})
}

func WithCallerKey(key string) EncoderOption {
	type CallerKeySetter interface{ setCallerKey(key string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(CallerKeySetter); is {
			setter.setCallerKey(key)
		}
	})
}

func WithFunctionKey(key string) EncoderOption {
	type FunctionKeySetter interface{ setFunctionKey(key string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(FunctionKeySetter); is {
			setter.setFunctionKey(key)
		}
	})
}

func WithStacktraceKey(key string) EncoderOption {
	type StacktraceKeySetter interface{ setStacktraceKey(key string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(StacktraceKeySetter); is {
			setter.setStacktraceKey(key)
		}
	})
}

func WithArrayStartSpace(space int) EncoderOption {
	type ArrayStartSpaceSetter interface{ setArrayStartSpace(space int) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(ArrayStartSpaceSetter); is {
			setter.setArrayStartSpace(space)
		}
	})
}

func WithArrayEndSpace(space int) EncoderOption {
	type ArrayEndSpaceSetter interface{ setArrayEndSpace(space int) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(ArrayEndSpaceSetter); is {
			setter.setArrayEndSpace(space)
		}
	})
}

func WithArraySpace(space int) EncoderOption {
	return option.Set[Encoder](func(target Encoder) {
		WithArrayStartSpace(space).Apply(target)
		WithArrayEndSpace(space).Apply(target)
	})
}

func WithMapStartSpace(space int) EncoderOption {
	type MapStartSpaceSetter interface{ setMapStartSpace(space int) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(MapStartSpaceSetter); is {
			setter.setMapStartSpace(space)
		}
	})
}

func WithMapEndSpace(space int) EncoderOption {
	type MapEndSpaceSetter interface{ setMapEndSpace(space int) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(MapEndSpaceSetter); is {
			setter.setMapEndSpace(space)
		}
	})
}

func WithMapSpace(space int) EncoderOption {
	return option.Set[Encoder](func(target Encoder) {
		WithMapStartSpace(space).Apply(target)
		WithMapEndSpace(space).Apply(target)
	})
}

func WithNamespaceSpace(space int) EncoderOption {
	return option.Set[Encoder](func(target Encoder) {
		WithArraySpace(space).Apply(target)
		WithMapSpace(space).Apply(target)
	})
}

func WithKeyValueSpace(space int) EncoderOption {
	type KeyValueSpaceSetter interface{ setKeyValueSpace(space int) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(KeyValueSpaceSetter); is {
			setter.setKeyValueSpace(space)
		}
	})
}

func WithElemSpace(space int) EncoderOption {
	type ElemSpaceSetter interface{ setElemSpace(space int) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(ElemSpaceSetter); is {
			setter.setElemSpace(space)
		}
	})
}

func WithSkipLineEnding(skip bool) EncoderOption {
	type SkipLineEndingSetter interface{ setSkipLineEnding(skip bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(SkipLineEndingSetter); is {
			setter.setSkipLineEnding(skip)
		}
	})
}

func invalidLineEndingChar(r rune) bool {
	return r != '\r' && r != '\n'
}

func CheckLineEnding(lineEnding string) error {
	if strings.IndexFunc(lineEnding, invalidLineEndingChar) > 0 || len(lineEnding) == 0 {
		return InvalidLineEndingError{lineEnding}
	}
	return nil
}

func WithLineEnding(lineEnding string) EncoderOption {
	assert.MustSuccess(CheckLineEnding(lineEnding))
	type LineEndingSetter interface{ setLineEnding(lineEnding string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(LineEndingSetter); is {
			setter.setLineEnding(lineEnding)
		}
	})
}

var (
	WithCRLineEnding      = WithLineEnding(LineEndingCR)
	WithLFLineEnding      = WithLineEnding(LineEndingLF)
	WithCRLFLineEnding    = WithLineEnding(LineEndingCRLF)
	WithDefaultLineEnding = WithLineEnding(DefaultLineEnding.Get())
)

func invalidIndentChar(r rune) bool {
	return r != ' ' && r != '\t'
}

func CheckIndent(indent string) error {
	if strings.IndexFunc(indent, invalidIndentChar) > 0 {
		return InvalidIndentError{indent}
	}
	return nil
}

func WithIndent(indent string) EncoderOption {
	assert.MustSuccess(CheckIndent(indent))
	type IndentSetter interface{ setIndent(indent string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(IndentSetter); is {
			setter.setIndent(indent)
		}
	})
}

var (
	WithJson2SpaceIndent  = WithIndent(Json2SpaceIndent)
	WithJson4SpaceIndent  = WithIndent(Json4SpaceIndent)
	WithJsonTabIndent     = WithIndent(JsonTabIndent)
	WithJsonDefaultIndent = WithIndent(JsonDefaultIndent)
)

func CheckIndentPrefix(prefix string) error {
	if strings.IndexFunc(prefix, invalidIndentChar) > 0 {
		return InvalidIndentPrefixError{prefix}
	}
	return nil
}

func WithIndentPrefix(prefix string) EncoderOption {
	assert.MustSuccess(CheckIndentPrefix(prefix))
	type IndentPrefixSetter interface{ setIndentPrefix(prefix string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(IndentPrefixSetter); is {
			setter.setIndentPrefix(prefix)
		}
	})
}

func WithEscapeHTML(escape bool) EncoderOption {
	type EscapeHTMLSetter interface{ setEscapeHTML(escape bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EscapeHTMLSetter); is {
			setter.setEscapeHTML(escape)
		}
	})
}

func WithEscapeESC(escape bool) EncoderOption {
	type EscapeESCSetter interface{ setEscapeESC(escape bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EscapeESCSetter); is {
			setter.setEscapeESC(escape)
		}
	})
}

func WithLevelFormatter(formatter LevelFormatter) EncoderOption {
	type LevelFormatterSetter interface {
		setLevelFormatter(formatter LevelFormatter)
	}
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(LevelFormatterSetter); is {
			setter.setLevelFormatter(formatter)
		}
	})
}

var (
	WithLowercaseLevelFormatter      = WithLevelFormatter(LowercaseLevelFormatter)
	WithLowercaseColorLevelFormatter = WithLevelFormatter(LowercaseColorLevelFormatter)
	WithCapitalLevelFormatter        = WithLevelFormatter(CapitalLevelFormatter)
	WithCapitalColorLevelFormatter   = WithLevelFormatter(CapitalColorLevelFormatter)
)

func WithTimeFormatter(formatter TimeFormatter) EncoderOption {
	type TimeFormatterSetter interface {
		setTimeFormatter(formatter TimeFormatter)
	}
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(TimeFormatterSetter); is {
			setter.setTimeFormatter(formatter)
		}
	})
}

func WithLayoutTimeFormatter(layout string) EncoderOption {
	return WithTimeFormatter(TimeFormatterOfLayout(layout))
}

var (
	WithEpochTimeFormatter       = WithTimeFormatter(EpochTimeFormatter)
	WithEpochMillisTimeFormatter = WithTimeFormatter(EpochMillisTimeFormatter)
	WithEpochNanosTimeFormatter  = WithTimeFormatter(EpochNanosTimeFormatter)
	WithDefaultTimeFormatter     = WithTimeFormatter(DefaultTimeFormatter)
	WithISO8601TimeFormatter     = WithTimeFormatter(ISO8601TimeFormatter)
	WithRFC3339TimeFormatter     = WithTimeFormatter(RFC3339TimeFormatter)
	WithRFC3339NanoTimeFormatter = WithTimeFormatter(RFC3339NanoTimeFormatter)
)

func WithDurationFormatter(formatter DurationFormatter) EncoderOption {
	type DurationFormatterSetter interface {
		setDurationFormatter(formatter DurationFormatter)
	}
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(DurationFormatterSetter); is {
			setter.setDurationFormatter(formatter)
		}
	})
}

var (
	WithSecondDurationFormatter = WithDurationFormatter(SecondDurationFormatter)
	WithNanosDurationFormatter  = WithDurationFormatter(NanosDurationFormatter)
	WithMillisDurationFormatter = WithDurationFormatter(MillisDurationFormatter)
	WithStringDurationFormatter = WithDurationFormatter(StringDurationFormatter)
)

func WithCallerFormatter(formatter CallerFormatter) EncoderOption {
	type CallerFormatterSetter interface {
		setCallerFormatter(formatter CallerFormatter)
	}
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(CallerFormatterSetter); is {
			setter.setCallerFormatter(formatter)
		}
	})
}

var (
	WithFullCallerFormatter  = WithCallerFormatter(FullCallerFormatter)
	WithShortCallerFormatter = WithCallerFormatter(ShortCallerFormatter)
)

func WithNameFormatter(formatter NameFormatter) EncoderOption {
	type NameFormatterSetter interface{ setNameFormatter(formatter NameFormatter) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(NameFormatterSetter); is {
			setter.setNameFormatter(formatter)
		}
	})
}

var WithFullNameFormatter = WithNameFormatter(FullNameFormatter)

func WithEncoderErrorHandler(handler EncoderErrorHandler) EncoderOption {
	type EncoderErrorHandlerSetter interface {
		setErrorHandler(handler EncoderErrorHandler)
	}
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EncoderErrorHandlerSetter); is {
			setter.setErrorHandler(handler)
		}
	})
}

func WithEncodeMessage(enable bool) EncoderOption {
	type EncodeMessageSetter interface{ setEncodeMessage(enable bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EncodeMessageSetter); is {
			setter.setEncodeMessage(enable)
		}
	})
}

func WithEncodeLevel(enable bool) EncoderOption {
	type EncodeLevelSetter interface{ setEncodeLevel(enable bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EncodeLevelSetter); is {
			setter.setEncodeLevel(enable)
		}
	})
}

func WithEncodeTime(enable bool) EncoderOption {
	type EncodeTimeSetter interface{ setEncodeTime(enable bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EncodeTimeSetter); is {
			setter.setEncodeTime(enable)
		}
	})
}

func WithEncodeName(enable bool) EncoderOption {
	type EncodeNameSetter interface{ setEncodeName(enable bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EncodeNameSetter); is {
			setter.setEncodeName(enable)
		}
	})
}

func WithEncodeCaller(enable bool) EncoderOption {
	type EncodeCallerSetter interface{ setEncodeCaller(enable bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EncodeCallerSetter); is {
			setter.setEncodeCaller(enable)
		}
	})
}

func WithEncodeFunction(enable bool) EncoderOption {
	type EncodeFunctionSetter interface{ setEncodeFunction(enable bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EncodeFunctionSetter); is {
			setter.setEncodeFunction(enable)
		}
	})
}

func WithEncodeStacktrace(enable bool) EncoderOption {
	type EncodeStacktraceSetter interface{ setEncodeStacktrace(enable bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(EncodeStacktraceSetter); is {
			setter.setEncodeStacktrace(enable)
		}
	})
}

var (
	WithEnableMessage     = WithEncodeMessage(true)
	WithEnableLevel       = WithEncodeLevel(true)
	WithEnableTime        = WithEncodeTime(true)
	WithEnableName        = WithEncodeName(true)
	WithEnableCaller      = WithEncodeCaller(true)
	WithEnableFunction    = WithEncodeFunction(true)
	WithEnableStacktrace  = WithEncodeStacktrace(true)
	WithDisableMessage    = WithEncodeMessage(false)
	WithDisableLevel      = WithEncodeLevel(false)
	WithDisableTime       = WithEncodeTime(false)
	WithDisableName       = WithEncodeName(false)
	WithDisableCaller     = WithEncodeCaller(false)
	WithDisableFunction   = WithEncodeFunction(false)
	WithDisableStacktrace = WithEncodeStacktrace(false)
)

func WithConsoleSeparator(sep string) EncoderOption {
	type ConsoleSeparatorSetter interface{ setConsoleSeparator(sep string) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(ConsoleSeparatorSetter); is {
			setter.setConsoleSeparator(sep)
		}
	})
}

func WithJsonIndentNewLine(enable bool) EncoderOption {
	type JsonIndentNewLineSetter interface{ setJsonIndentNewLine(enable bool) }
	return option.Set[Encoder](func(target Encoder) {
		if setter, is := target.(JsonIndentNewLineSetter); is {
			setter.setJsonIndentNewLine(enable)
		}
	})
}
