package log

import (
	"encoding/base64"
	"encoding/json"
	"gitee.com/csstx2020/cvds2020/go-log/internal/bufferpool"
	"reflect"
)

type Encoder interface {
	WithOptions(options ...EncoderOption) Encoder
	Encode(Logger, *Entry, ...Field) (any, error)
}

type EncoderErrorHandler func(Encoder, Logger, error)

func DefaultEncoderErrorHandler(encoder Encoder, logger Logger, err error) {
	if !logger.IsNop() {
		logger.Error("日志编码失败", Stringer("编码器类型", reflect.TypeOf(encoder)), Err("错误", err))
	}
}

func encodeNil(buffer *bufferpool.Buffer) {
	buffer.AppendString("<nil>")
}

func encodeBinary(buffer *bufferpool.Buffer, val []byte) error {
	base64Enc := base64.NewEncoder(base64.StdEncoding, buffer)
	if _, err := base64Enc.Write(val); err != nil {
		return err
	}
	if err := base64Enc.Close(); err != nil {
		return err
	}
	return nil
}

func encodeBool(buffer *bufferpool.Buffer, val bool) error {
	if val {
		buffer.AppendString("true")
	} else {
		buffer.AppendString("false")
	}
	return nil
}

func encodeComplex(buffer *bufferpool.Buffer, val complex128, precision int) {
	r, i := real(val), imag(val)
	buffer.AppendFloat(r, precision)
	if i >= 0 {
		buffer.AppendByte('+')
	}
	buffer.AppendFloat(i, precision)
	buffer.AppendByte('i')
}

func encodeObject(buffer *bufferpool.Buffer, val any) error {
	je := json.NewEncoder(buffer)
	return je.Encode(val)
}
