package log

import (
	"gitee.com/csstx2020/cvds2020/go-log/internal/bufferpool"
	"strings"
	"time"
)

type Entry struct {
	Level      LevelI
	Time       time.Time
	LoggerName string
	Message    string
	Caller     EntryCaller
	Stack      string
	Fields     []Field
}

type EntryCaller struct {
	Defined  bool
	PC       uintptr
	File     string
	Line     int
	Function string
}

func (ec EntryCaller) FullPath() string {
	if !ec.Defined {
		return "undefined"
	}
	buf := bufferpool.Get()
	buf.AppendString(ec.File)
	buf.AppendByte(':')
	buf.AppendInt(int64(ec.Line))
	caller := buf.String()
	buf.Free()
	return caller
}

func (ec EntryCaller) TrimmedPath() string {
	if !ec.Defined {
		return "undefined"
	}
	// 为了确保在Windows上正确裁剪路径，我们需要使用正斜杠'/'而不是os.PathSeparator。
	// 这是因为给定的路径来自Go标准库中的runtime.Caller()函数，即使在Windows上（截至
	// 2021年9月），该函数返回的路径也是使用正斜杠'/'而不是Windows上的反斜杠。
	//
	// 参考	https://github.com/golang/go/issues/3335
	// 和	https://github.com/golang/go/issues/18151
	idx := strings.LastIndexByte(ec.File, '/')
	if idx == -1 {
		return ec.FullPath()
	}
	// 找到倒数第二个分隔符。
	idx = strings.LastIndexByte(ec.File[:idx], '/')
	if idx == -1 {
		return ec.FullPath()
	}
	buf := bufferpool.Get()
	// 保留倒数第二个分隔符后的所有内容。
	buf.AppendString(ec.File[idx+1:])
	buf.AppendByte(':')
	buf.AppendInt(int64(ec.Line))
	caller := buf.String()
	buf.Free()
	return caller
}

func (ec EntryCaller) String() string {
	return ec.FullPath()
}
