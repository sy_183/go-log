package log

import (
	"fmt"
	"gitee.com/csstx2020/cvds2020/go-common/generic"
	"reflect"
)

type SizeOutOfRangeError[N generic.Integer] struct {
	Target string
	Size   N
	Min    N
	Max    N
}

func NewSizeOutOfRangeError[N generic.Integer](target string, size, min, max N) SizeOutOfRangeError[N] {
	return SizeOutOfRangeError[N]{Target: target, Size: size, Min: min, Max: max}
}

func (e SizeOutOfRangeError[N]) Error() string {
	return fmt.Sprintf("%s size(%d) out of range(%d, %d)", e.Target, e.Size, e.Min, e.Max)
}

type UnknownError[V any] struct {
	Target string
	Value  V
}

func NewUnknownError[V any](target string, value V) UnknownError[V] {
	return UnknownError[V]{Target: target, Value: value}
}

func (e UnknownError[V]) Error() string {
	return fmt.Sprintf("unknown %s(%v)", e.Target, e.Value)
}

type UnmarshalNilError struct {
	Type string
}

func NewUnmarshalNilError(v any) UnmarshalNilError {
	return UnmarshalNilError{reflect.TypeOf(v).String()}
}

func (e UnmarshalNilError) Error() string {
	return fmt.Sprintf("cannot unmarshal a nil %s", e.Type)
}

type (
	InvalidIndentError       struct{ Indent string }
	InvalidIndentPrefixError struct{ IndentPrefix string }
	InvalidLineEndingError   struct{ LineEnding string }
)

func (e InvalidIndentError) Error() string {
	return fmt.Sprintf(`invalid indent(%s), indent can only contain space(' ') or tab('\t')`, e.Indent)
}

func (e InvalidIndentPrefixError) Error() string {
	return fmt.Sprintf(`invalid indent prefix(%s), indent prefix can only contain space(' ') or tab('\t')`, e.IndentPrefix)
}

func (e InvalidLineEndingError) Error() string {
	return fmt.Sprintf(`invalid line ending(%s), line ending can only contain CR('\r') or LF('\n')`, e.LineEnding)
}
