package log

import (
	"fmt"
	"time"
)

type Field struct {
	Key string
	Value
}

func field(key string, value Value) Field { return Field{Key: key, Value: value} }

var Skip = Field{Value: SkipV}

func Nil(key string) Field                                   { return field(key, NilV) }
func Binary(key string, val []byte) Field                    { return field(key, BinaryV(val)) }
func BinaryP(key string, val *[]byte) Field                  { return field(key, BinaryPV(val)) }
func Bool(key string, val bool) Field                        { return field(key, BoolV(val)) }
func BoolP(key string, val *bool) Field                      { return field(key, BoolPV(val)) }
func Complex128(key string, val complex128) Field            { return field(key, Complex128V(val)) }
func Complex128P(key string, val *complex128) Field          { return field(key, Complex128PV(val)) }
func Complex64(key string, val complex64) Field              { return field(key, Complex64V(val)) }
func Complex64P(key string, val *complex64) Field            { return field(key, Complex64PV(val)) }
func Float64(key string, val float64) Field                  { return field(key, Float64V(val)) }
func Float64P(key string, val *float64) Field                { return field(key, Float64PV(val)) }
func Float32(key string, val float32) Field                  { return field(key, Float32V(val)) }
func Float32P(key string, val *float32) Field                { return field(key, Float32PV(val)) }
func Int(key string, val int) Field                          { return field(key, IntV(val)) }
func IntP(key string, val *int) Field                        { return field(key, IntPV(val)) }
func Int64(key string, val int64) Field                      { return field(key, Int64V(val)) }
func Int64P(key string, val *int64) Field                    { return field(key, Int64PV(val)) }
func Int32(key string, val int32) Field                      { return field(key, Int32V(val)) }
func Int32P(key string, val *int32) Field                    { return field(key, Int32PV(val)) }
func Int16(key string, val int16) Field                      { return field(key, Int16V(val)) }
func Int16P(key string, val *int16) Field                    { return field(key, Int16PV(val)) }
func Int8(key string, val int8) Field                        { return field(key, Int8V(val)) }
func Int8P(key string, val *int8) Field                      { return field(key, Int8PV(val)) }
func String(key string, val string) Field                    { return field(key, StringV(val)) }
func StringP(key string, val *string) Field                  { return field(key, StringPV(val)) }
func Uint(key string, val uint) Field                        { return field(key, UintV(val)) }
func UintP(key string, val *uint) Field                      { return field(key, UintPV(val)) }
func Uint64(key string, val uint64) Field                    { return field(key, Uint64V(val)) }
func Uint64P(key string, val *uint64) Field                  { return field(key, Uint64PV(val)) }
func Uint32(key string, val uint32) Field                    { return field(key, Uint32V(val)) }
func Uint32P(key string, val *uint32) Field                  { return field(key, Uint32PV(val)) }
func Uint16(key string, val uint16) Field                    { return field(key, Uint16V(val)) }
func Uint16P(key string, val *uint16) Field                  { return field(key, Uint16PV(val)) }
func Uint8(key string, val uint8) Field                      { return field(key, Uint8V(val)) }
func Uint8P(key string, val *uint8) Field                    { return field(key, Uint8PV(val)) }
func Uintptr(key string, val uintptr) Field                  { return field(key, UintptrV(val)) }
func UintptrP(key string, val *uintptr) Field                { return field(key, UintptrPV(val)) }
func Time(key string, val time.Time) Field                   { return field(key, TimeV(val)) }
func TimeP(key string, val *time.Time) Field                 { return field(key, TimePV(val)) }
func Duration(key string, val time.Duration) Field           { return field(key, DurationV(val)) }
func DurationP(key string, val *time.Duration) Field         { return field(key, DurationPV(val)) }
func Stringer(key string, val fmt.Stringer) Field            { return field(key, StringerV(val)) }
func Formatter(key string, val ValueFormatter) Field         { return field(key, FormatterV(val)) }
func Err(key string, val error) Field                        { return field(key, ErrorV(val)) }
func Object(key string, val any) Field                       { return field(key, ObjectV(val)) }
func Slice[V any](key string, val []V) Field                 { return field(key, SliceV(val)) }
func Map[K comparable, V any](key string, val map[K]V) Field { return field(key, MapV(val)) }
func Values(key string, values ...Value) Field               { return field(key, ValuesV(values...)) }
func Fields(key string, fields ...Field) Field               { return field(key, FieldsV(fields...)) }
func Any(key string, val any) Field                          { return field(key, AnyV(val)) }
