package log

import (
	flagPkg "gitee.com/csstx2020/cvds2020/go-common/flag"
	"gitee.com/csstx2020/cvds2020/go-common/lock"
	"os"
	"path/filepath"
	"sync"
)

type fileCore struct {
	sink   *AtomicSink
	path   string
	flag   int
	mode   os.FileMode
	closed bool
	mu     sync.Mutex
}

var defaultFilePath string

func init() {
	program := "log"
	if len(os.Args) > 0 {
		program = filepath.Base(os.Args[0])
	}
	defaultFilePath = program + ".log"
}

func newFileCore(path string) fileCore {
	if path == "" {
		path = defaultFilePath
	}
	return fileCore{
		sink: NewAtomicSink(nil),
		path: path,
		flag: os.O_CREATE | os.O_WRONLY | os.O_APPEND,
		mode: 0644,
	}
}

func (c *fileCore) setPath(name string)      { c.path = name }
func (c *fileCore) setFlag(flag int)         { c.flag = flag }
func (c *fileCore) setMode(mode os.FileMode) { c.mode = mode }

func (c *fileCore) Open() error {
	var closed bool
	path, flag, mode := lock.LockGetTriple(&c.mu, func() (string, int, os.FileMode) {
		closed = c.closed
		return c.path, c.flag, c.mode
	})
	if closed {
		return os.ErrClosed
	}
	file, err := os.OpenFile(path, flag, mode)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(filepath.Dir(path), 0755)
			if err == nil {
				file, err = os.OpenFile(path, flag, mode)
			}
		}
		if err != nil {
			return err
		}
	}
	old := lock.LockGet(&c.mu, func() (old Sink) {
		old = c.sink.Sink()
		if flagPkg.TestFlag(flag, os.O_SYNC) {
			c.sink.SetSink(WriterCloserSink(file))
		} else {
			c.sink.SetSink(file)
		}
		return
	})
	if old != nil {
		old.Close()
	}
	return nil
}

func (c *fileCore) SetPath(path string) {
	lock.LockDo(&c.mu, func() {
		c.path = path
	})
}

func (c *fileCore) SetFile(path string, flag int, mode os.FileMode) {
	lock.LockDo(&c.mu, func() {
		c.path, c.flag, c.mode = path, flag, mode
	})
}

func (c *fileCore) Close() error {
	sink := lock.LockGet(&c.mu, func() (old Sink) {
		if c.closed {
			return nil
		}
		old = c.sink.Sink()
		c.sink.SetSink(nil)
		c.closed = true
		return
	})
	if sink != nil {
		return sink.Close()
	}
	return nil
}

type FileCore struct {
	fileCore
	SinkCore
}

func (c *FileCore) Close() error {
	return c.fileCore.Close()
}

func NewFileCore(path string, options ...CoreOption) *FileCore {
	c := &FileCore{
		fileCore: newFileCore(path),
		SinkCore: newSinkCore(nil),
	}
	c.SinkCore.sink = c.fileCore.sink
	c.SinkCore.init(c, options...)
	return c
}

func ProvideFileCore(options ...CoreOption) Core {
	return NewFileCore("", options...)
}

func (c *FileCore) setSink(sink Sink) {}

func (c *FileCore) Open() error { return c.fileCore.Open() }

type AsyncFileCore struct {
	fileCore
	AsyncSinkCore
}

func NewAsyncFileCore(path string, options ...CoreOption) *AsyncFileCore {
	c := &AsyncFileCore{
		fileCore:      newFileCore(path),
		AsyncSinkCore: newAsyncSinkCore(nil),
	}
	c.AsyncSinkCore.sink = c.fileCore.sink
	c.AsyncSinkCore.init(c, options...)
	return c
}

func ProvideAsyncFileCore(options ...CoreOption) Core {
	return NewAsyncFileCore("", options...)
}

func (c *AsyncFileCore) setSink(sink Sink) {}

func (c *AsyncFileCore) Open() error { return c.fileCore.Open() }

func (c *AsyncFileCore) Close() error {
	if c.AsyncSinkCore.closeQueueWait() {
		return c.fileCore.Close()
	}
	return nil
}
