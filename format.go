package log

import (
	"fmt"
	"gitee.com/csstx2020/cvds2020/go-common/unsafe"
	"gitee.com/csstx2020/cvds2020/go-log/internal/bufferpool"
	"strings"
	"sync"
	"time"
)

var argContextPool = sync.Pool{
	New: func() interface{} {
		return new(argContext)
	},
}

func getArgContext() *argContext {
	return argContextPool.Get().(*argContext)
}

func freeArgContext(ctx *argContext) {
	ctx.values = ctx.values[:0]
	ctx.fields = ctx.fields[:0]
	ctx.fieldUsed = ctx.fieldUsed[:0]
	if ctx.fieldMap != nil {
		for key := range ctx.fieldMap {
			delete(ctx.fieldMap, key)
		}
	}
}

type argContext struct {
	values    []Value
	fields    []Field
	fieldUsed []bool
	fieldMap  map[string]int
}

func (c *argContext) get(key string) (value Value) {
	if len(c.fields) > 8 {
		if i, ok := c.fieldMap[key]; ok {
			c.fieldUsed[i] = true
			return c.fields[i].Value
		}
		return
	}
	for i := range c.fields {
		if c.fields[i].Key == key {
			c.fieldUsed[i] = true
			return c.fields[i].Value
		}
	}
	return Value{}
}

func parseArgs(args ...any) *argContext {
	ctx := getArgContext()
	for _, arg := range args {
		switch ra := arg.(type) {
		case Field:
			ctx.fields = append(ctx.fields, ra)
			ctx.fieldUsed = append(ctx.fieldUsed, false)
		case Value:
			ctx.values = append(ctx.values, ra)
		default:
			ctx.values = append(ctx.values, AnyV(arg))
		}
	}
	if len(ctx.fields) > 8 {
		if ctx.fieldMap == nil {
			ctx.fieldMap = make(map[string]int)
		}
		for i := range ctx.fields {
			ctx.fieldMap[ctx.fields[i].Key] = i
		}
	}
	return ctx
}

func parseFmtKey(fmtKey string) (f string, key string) {
	fmtKey = strings.TrimSpace(fmtKey)
	if fmtKey == "" {
		return "", ""
	}
	if fmtKey[0] == '%' {
		if len(fmtKey) == 1 {
			return "", fmtKey
		}
		switch fmtKey[1] {
		case '%':
			return "", fmtKey[1:]
		case '(':
			fmtKey = fmtKey[2:]
			i := strings.LastIndexByte(fmtKey, ')')
			if i < 0 {
				return fmtKey, ""
			}
			f = fmtKey[:i]
			fmtKey = fmtKey[i+1:]
			if i = strings.IndexByte(fmtKey, ':'); i >= 0 {
				return f, fmtKey[i+1:]
			}
			return f, ""
		default:
			if i := strings.IndexByte(fmtKey, ':'); i > 0 {
				return fmtKey[:i], fmtKey[i+1:]
			}
			return fmtKey, ""
		}
	}
	return "", fmtKey
}

var formatJsonEncoder = newJsonEncoder(WithEncoderErrorHandler(nil))

func formatBinary(buffer *bufferpool.Buffer, val []byte, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		encodeBinary(buffer, val)
	}
}

func formatBool(buffer *bufferpool.Buffer, val bool, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		encodeBool(buffer, val)
	}
}

func formatComplex128(buffer *bufferpool.Buffer, val complex128, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		encodeComplex(buffer, val, 64)
	}
}

func formatComplex64(buffer *bufferpool.Buffer, val complex64, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		encodeComplex(buffer, complex128(val), 32)
	}
}

func formatFloat64(buffer *bufferpool.Buffer, val float64, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendFloat(val, 64)
	}
}

func formatFloat32(buffer *bufferpool.Buffer, val float32, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendFloat(float64(val), 32)
	}
}

func formatInt64(buffer *bufferpool.Buffer, val int64, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendInt(val)
	}
}

func formatInt32(buffer *bufferpool.Buffer, val int32, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendInt(int64(val))
	}
}

func formatInt16(buffer *bufferpool.Buffer, val int16, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendInt(int64(val))
	}
}

func formatInt8(buffer *bufferpool.Buffer, val int8, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendInt(int64(val))
	}
}

func formatString(buffer *bufferpool.Buffer, val string, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendString(val)
	}
}

func formatUint64(buffer *bufferpool.Buffer, val uint64, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendUint(val)
	}
}

func formatUint32(buffer *bufferpool.Buffer, val uint32, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendUint(uint64(val))
	}
}

func formatUint16(buffer *bufferpool.Buffer, val uint16, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendUint(uint64(val))
	}
}

func formatUint8(buffer *bufferpool.Buffer, val uint8, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendUint(uint64(val))
	}
}

func formatUintptr(buffer *bufferpool.Buffer, val uintptr, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		buffer.AppendUint(uint64(val))
	}
}

func formatTime(buffer *bufferpool.Buffer, val time.Time, f string) {
	switch f {
	case "rfc3339nano", "RFC3339Nano":
		buffer.AppendString(val.Format(time.RFC3339Nano))
	case "rfc3339", "RFC3339":
		buffer.AppendString(val.Format(time.RFC3339))
	case "iso8601", "ISO8601":
		buffer.AppendString(val.Format(ISO8601TimeLayout))
	case "sec", "second":
		buffer.AppendFloat(timeSecond(val), 64)
	case "ms", "millis":
		buffer.AppendFloat(timeMillis(val), 64)
	case "ns", "nanos":
		buffer.AppendInt(val.UnixNano())
	case "", "default":
		buffer.AppendString(val.Format(DefaultTimeLayout))
	default:
		buffer.AppendString(val.Format(f))
	}
}

func formatDuration(buffer *bufferpool.Buffer, val time.Duration, f string) {
	switch f {
	case "string":
		buffer.AppendString(val.String())
	case "ns", "nanos":
		buffer.AppendInt(int64(val))
	case "ms", "millis":
		buffer.AppendInt(val.Nanoseconds() / 1e6)
	default:
		buffer.AppendFloat(durationSecond(val), 64)
	}
}

func formatFormatter(buffer *bufferpool.Buffer, val ValueFormatter, f string) {
	formatValue(buffer, val.Format(), f)
}

func formatError(buffer *bufferpool.Buffer, err error, f string) {
	switch re := err.(type) {
	case ValueFormatter:
		formatFormatter(buffer, re, f)
	default:
		formatString(buffer, re.Error(), f)
	}
}

func formatObject(buffer *bufferpool.Buffer, val any, f string) {
	if f != "" && f[0] == '%' {
		buffer.AppendString(fmt.Sprintf(f, val))
	} else {
		encodeObject(buffer, val)
	}
}

func formatValues(buffer *bufferpool.Buffer, val []Value, f string) {
	ctx := unsafe.Unescape(&jsonEncoderCtx{
		jsonEncoderConfig: formatJsonEncoder.jsonEncoderConfig,
		buffer:            buffer,
	})
	ctx.encodeValues(val)
}

func formatFields(buffer *bufferpool.Buffer, val []Field, f string) {
	ctx := unsafe.Unescape(&jsonEncoderCtx{
		jsonEncoderConfig: formatJsonEncoder.jsonEncoderConfig,
		buffer:            buffer,
	})
	ctx.encodeFields(val)
}

func formatSkipV(buffer *bufferpool.Buffer, value Value, f string) {}
func formatNilV(buffer *bufferpool.Buffer, value Value, f string) {
	encodeNil(buffer)
}
func formatBinaryV(buffer *bufferpool.Buffer, value Value, f string) {
	formatBinary(buffer, value.Binary(), f)
}
func formatBoolV(buffer *bufferpool.Buffer, value Value, f string) {
	formatBool(buffer, value.Bool(), f)
}
func formatComplex128V(buffer *bufferpool.Buffer, value Value, f string) {
	formatComplex128(buffer, value.Complex128(), f)
}
func formatComplex64V(buffer *bufferpool.Buffer, value Value, f string) {
	formatComplex64(buffer, value.Complex64(), f)
}
func formatFloat64V(buffer *bufferpool.Buffer, value Value, f string) {
	formatFloat64(buffer, value.Float64(), f)
}
func formatFloat32V(buffer *bufferpool.Buffer, value Value, f string) {
	formatFloat32(buffer, value.Float32(), f)
}
func formatInt64V(buffer *bufferpool.Buffer, value Value, f string) {
	formatInt64(buffer, value.Int64(), f)
}
func formatInt32V(buffer *bufferpool.Buffer, value Value, f string) {
	formatInt32(buffer, value.Int32(), f)
}
func formatInt16V(buffer *bufferpool.Buffer, value Value, f string) {
	formatInt16(buffer, value.Int16(), f)
}
func formatInt8V(buffer *bufferpool.Buffer, value Value, f string) {
	formatInt8(buffer, value.Int8(), f)
}
func formatStringV(buffer *bufferpool.Buffer, value Value, f string) {
	formatString(buffer, value.String(), f)
}
func formatUint64V(buffer *bufferpool.Buffer, value Value, f string) {
	formatUint64(buffer, value.Uint64(), f)
}
func formatUint32V(buffer *bufferpool.Buffer, value Value, f string) {
	formatUint32(buffer, value.Uint32(), f)
}
func formatUint16V(buffer *bufferpool.Buffer, value Value, f string) {
	formatUint16(buffer, value.Uint16(), f)
}
func formatUint8V(buffer *bufferpool.Buffer, value Value, f string) {
	formatUint8(buffer, value.Uint8(), f)
}
func formatUintptrV(buffer *bufferpool.Buffer, value Value, f string) {
	formatUintptr(buffer, value.Uintptr(), f)
}
func formatTimeV(buffer *bufferpool.Buffer, value Value, f string) {
	formatTime(buffer, value.Time(), f)
}
func formatTimeFullV(buffer *bufferpool.Buffer, value Value, f string) {
	formatTime(buffer, value.TimeFull(), f)
}
func formatDurationV(buffer *bufferpool.Buffer, value Value, f string) {
	formatDuration(buffer, value.Duration(), f)
}
func formatStringerV(buffer *bufferpool.Buffer, value Value, f string) {
	formatString(buffer, value.Stringer().String(), f)
}
func formatFormatterV(buffer *bufferpool.Buffer, value Value, f string) {
	formatFormatter(buffer, value.Formatter(), f)
}
func formatErrorV(buffer *bufferpool.Buffer, value Value, f string) {
	formatError(buffer, value.Error(), f)
}
func formatObjectV(buffer *bufferpool.Buffer, value Value, f string) {
	formatObject(buffer, value.Object(), f)
}
func formatValuesV(buffer *bufferpool.Buffer, value Value, f string) {
	formatValues(buffer, value.Values(), f)
}
func formatFieldsV(buffer *bufferpool.Buffer, value Value, f string) {
	formatFields(buffer, value.Fields(), f)
}

var formatValueMap [MaxTypes]func(buffer *bufferpool.Buffer, value Value, f string)

func init() {
	formatValueMap = [MaxTypes]func(buffer *bufferpool.Buffer, value Value, f string){
		SkipType:       formatSkipV,
		NilType:        formatNilV,
		BinaryType:     formatBinaryV,
		BoolType:       formatBoolV,
		Complex128Type: formatComplex128V,
		Complex64Type:  formatComplex64V,
		Float64Type:    formatFloat64V,
		Float32Type:    formatFloat32V,
		Int64Type:      formatInt64V,
		Int32Type:      formatInt32V,
		Int16Type:      formatInt16V,
		Int8Type:       formatInt8V,
		StringType:     formatStringV,
		Uint64Type:     formatUint64V,
		Uint32Type:     formatUint32V,
		Uint16Type:     formatUint16V,
		Uint8Type:      formatUint8V,
		UintptrType:    formatUintptrV,
		TimeType:       formatTimeV,
		TimeFullType:   formatTimeFullV,
		DurationType:   formatDurationV,
		StringerType:   formatStringerV,
		FormatterType:  formatFormatterV,
		ErrorType:      formatErrorV,
		ObjectType:     formatObjectV,
		SliceType:      formatObjectV,
		MapType:        formatObjectV,
		ValuesType:     formatValuesV,
		FieldsType:     formatFieldsV,
	}
}

func formatValue(buffer *bufferpool.Buffer, value Value, f string) {
	if value.Type < MaxTypes {
		if formatter := formatValueMap[value.Type]; formatter != nil {
			formatter(buffer, value, f)
		}
	}
}

func formatArgs(format string, args []any) (msg string, fields []Field) {
	ctx := parseArgs(args...)
	defer freeArgContext(ctx)
	var buffer *bufferpool.Buffer
	p := format
	expect := byte('{')
	local := 0
	for {
		if i := strings.IndexByte(p, expect); i >= 0 {
			if buffer == nil {
				buffer = bufferpool.Get()
			}
			if expect == '{' {
				buffer.AppendString(p[:i])
				if i < len(p)-1 {
					if p[i+1] == '{' {
						buffer.AppendByte('{')
						p = p[i+2:]
						continue
					}
				}
				p = p[i+1:]
				expect = '}'
			} else {
				f, key := parseFmtKey(p[:i])
				var value Value
				if key == "" {
					if len(ctx.values) > local {
						value = ctx.values[local]
						local++
					}
				} else {
					value = ctx.get(key)
				}
				formatValue(buffer, value, f)
				p = p[i+1:]
				expect = '{'
			}
		} else {
			break
		}
	}
	for i := range ctx.fields {
		if !ctx.fieldUsed[i] {
			fields = append(fields, ctx.fields[i])
		}
	}
	if buffer == nil {
		return format, fields
	} else {
		defer buffer.Free()
		if expect == '{' {
			buffer.AppendString(p)
		}
		return buffer.String(), fields
	}
}
