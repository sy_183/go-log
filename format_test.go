package log

import (
	"testing"
	"time"
)

func TestFormat(t *testing.T) {
	logger := New(
		WithDebug,
		WithConsoleEncoder(WithCapitalColorLevelFormatter, WithJsonDefaultIndent),
		WithStdoutCore,
		WithName("test"),
	)
	logger.Infof("test {%b} {%x} {%()} {} {} {}", 11, "hello", time.Now())
}
