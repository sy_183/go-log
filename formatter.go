package log

import (
	"time"
)

type ValueFormatter interface {
	Format() Value
}

type LevelFormatter func(LevelI) Value

func LowercaseLevelFormatter(l LevelI) Value      { return StringV(l.String()) }
func LowercaseColorLevelFormatter(l LevelI) Value { return StringV(l.ColorString()) }
func CapitalLevelFormatter(l LevelI) Value        { return StringV(l.CapitalString()) }
func CapitalColorLevelFormatter(l LevelI) Value   { return StringV(l.CapitalColorString()) }

const TargetLevelFormatter = "level formatter"

func (f *LevelFormatter) UnmarshalText(text []byte) error {
	if f == nil {
		return NewUnmarshalNilError(f)
	}
	switch s := string(text); s {
	case "lower", "":
		*f = LowercaseLevelFormatter
	case "lower-color", "color":
		*f = LowercaseColorLevelFormatter
	case "capital":
		*f = CapitalLevelFormatter
	case "capital-color":
		*f = CapitalColorLevelFormatter
	default:
		return NewUnknownError(TargetLevelFormatter, s)
	}
	return nil
}

type (
	lowercaseLevel      struct{ LevelI }
	lowercaseColorLevel struct{ LevelI }
	capitalLevel        struct{ LevelI }
	capitalColorLevel   struct{ LevelI }
)

func LowercaseLevel(l LevelI) ValueFormatter      { return lowercaseLevel{l} }
func LowercaseColorLevel(l LevelI) ValueFormatter { return lowercaseColorLevel{l} }
func CapitalLevel(l LevelI) ValueFormatter        { return capitalLevel{l} }
func CapitalColorLevel(l LevelI) ValueFormatter   { return capitalColorLevel{l} }

func (l lowercaseLevel) Format() Value      { return LowercaseLevelFormatter(l.LevelI) }
func (l lowercaseColorLevel) Format() Value { return LowercaseColorLevelFormatter(l.LevelI) }
func (l capitalLevel) Format() Value        { return CapitalLevelFormatter(l.LevelI) }
func (l capitalColorLevel) Format() Value   { return CapitalColorLevelFormatter(l.LevelI) }

type TimeFormatter func(time.Time) Value

const (
	DefaultTimeLayout = "2006-01-02 15:04:05.999999999"
	ISO8601TimeLayout = "2006-01-02T15:04:05.000Z0700"
)

func timeSecond(t time.Time) float64 { return float64(t.UnixNano()) / float64(time.Second) }
func timeMillis(t time.Time) float64 { return float64(t.UnixNano()) / float64(time.Millisecond) }

func layoutTimeValue(t time.Time, layout string) Value { return StringV(t.Format(layout)) }

func TimeFormatterOfLayout(layout string) TimeFormatter {
	return func(t time.Time) Value { return layoutTimeValue(t, layout) }
}

func EpochTimeFormatter(t time.Time) Value       { return Float64V(timeSecond(t)) }
func EpochMillisTimeFormatter(t time.Time) Value { return Float64V(timeMillis(t)) }
func EpochNanosTimeFormatter(t time.Time) Value  { return Int64V(t.UnixNano()) }
func DefaultTimeFormatter(t time.Time) Value     { return layoutTimeValue(t, DefaultTimeLayout) }
func ISO8601TimeFormatter(t time.Time) Value     { return layoutTimeValue(t, ISO8601TimeLayout) }
func RFC3339TimeFormatter(t time.Time) Value     { return layoutTimeValue(t, time.RFC3339) }
func RFC3339NanoTimeFormatter(t time.Time) Value { return layoutTimeValue(t, time.RFC3339Nano) }

const TargetTimeFormatter = "time formatter"

func (f *TimeFormatter) UnmarshalText(text []byte) error {
	if f == nil {
		return NewUnmarshalNilError(f)
	}
	switch s := string(text); s {
	case "rfc3339nano", "RFC3339Nano":
		*f = RFC3339NanoTimeFormatter
	case "rfc3339", "RFC3339":
		*f = RFC3339TimeFormatter
	case "iso8601", "ISO8601":
		*f = ISO8601TimeFormatter
	case "sec", "second":
		*f = EpochTimeFormatter
	case "ms", "millis":
		*f = EpochMillisTimeFormatter
	case "ns", "nanos":
		*f = EpochNanosTimeFormatter
	case "", "default":
		*f = DefaultTimeFormatter
	default:
		return NewUnknownError(TargetTimeFormatter, s)
	}
	return nil
}

type (
	EpochTime       time.Time
	EpochMillisTime time.Time
	EpochNanosTime  time.Time
	DefaultTime     time.Time
	ISO8601Time     time.Time
	RFC3339Time     time.Time
	RFC3339NanoTime time.Time
	LayoutTime      struct {
		time.Time
		Layout string
	}
)

func NewLayoutTime(t time.Time, layout string) LayoutTime {
	return LayoutTime{Time: t, Layout: layout}
}

func (t LayoutTime) Format() Value {
	return layoutTimeValue(t.Time, t.Layout)
}

func (t EpochTime) Format() Value       { return EpochTimeFormatter(time.Time(t)) }
func (t EpochMillisTime) Format() Value { return EpochMillisTimeFormatter(time.Time(t)) }
func (t EpochNanosTime) Format() Value  { return EpochNanosTimeFormatter(time.Time(t)) }
func (t DefaultTime) Format() Value     { return DefaultTimeFormatter(time.Time(t)) }
func (t ISO8601Time) Format() Value     { return ISO8601TimeFormatter(time.Time(t)) }
func (t RFC3339Time) Format() Value     { return RFC3339TimeFormatter(time.Time(t)) }
func (t RFC3339NanoTime) Format() Value { return RFC3339NanoTimeFormatter(time.Time(t)) }

type DurationFormatter func(time.Duration) Value

func durationSecond(d time.Duration) float64 { return float64(d) / float64(time.Second) }

func SecondDurationFormatter(d time.Duration) Value { return Float64V(durationSecond(d)) }
func NanosDurationFormatter(d time.Duration) Value  { return Int64V(int64(d)) }
func MillisDurationFormatter(d time.Duration) Value { return Int64V(d.Nanoseconds() / 1e6) }
func StringDurationFormatter(d time.Duration) Value { return StringV(d.String()) }

const TargetDurationFormatter = "duration formatter"

func (f *DurationFormatter) UnmarshalText(text []byte) error {
	if f == nil {
		return NewUnmarshalNilError(f)
	}
	switch s := string(text); s {
	case "string":
		*f = StringDurationFormatter
	case "ns", "nanos":
		*f = NanosDurationFormatter
	case "ms", "millis":
		*f = MillisDurationFormatter
	case "", "sec", "second":
		*f = SecondDurationFormatter
	default:
		return NewUnknownError(TargetDurationFormatter, s)
	}
	return nil
}

type (
	SecondDuration time.Duration
	NanosDuration  time.Duration
	MillisDuration time.Duration
	StringDuration time.Duration
)

func (d SecondDuration) Format() Value { return SecondDurationFormatter(time.Duration(d)) }
func (d NanosDuration) Format() Value  { return NanosDurationFormatter(time.Duration(d)) }
func (d MillisDuration) Format() Value { return MillisDurationFormatter(time.Duration(d)) }
func (d StringDuration) Format() Value { return StringDurationFormatter(time.Duration(d)) }

type CallerFormatter func(EntryCaller) Value

func FullCallerFormatter(caller EntryCaller) Value  { return StringV(caller.FullPath()) }
func ShortCallerFormatter(caller EntryCaller) Value { return StringV(caller.TrimmedPath()) }

const TargetCallerFormatter = "caller formatter"

func (f *CallerFormatter) UnmarshalText(text []byte) error {
	if f == nil {
		return NewUnmarshalNilError(f)
	}
	switch s := string(text); s {
	case "full":
		*f = FullCallerFormatter
	case "", "short":
		*f = ShortCallerFormatter
	default:
		return NewUnknownError(TargetCallerFormatter, s)
	}
	return nil
}

type (
	FullCaller  EntryCaller
	ShortCaller EntryCaller
)

func (c FullCaller) Format() Value  { return FullCallerFormatter(EntryCaller(c)) }
func (c ShortCaller) Format() Value { return ShortCallerFormatter(EntryCaller(c)) }

type NameFormatter func(name string) Value

func FullNameFormatter(name string) Value { return StringV(name) }

const TargetNameFormatter = "name formatter"

func (f *NameFormatter) UnmarshalText(text []byte) error {
	if f == nil {
		return NewUnmarshalNilError(f)
	}
	switch s := string(text); s {
	case "", "full":
		*f = FullNameFormatter
	default:
		return NewUnknownError(TargetNameFormatter, s)
	}
	return nil
}

type FullName string

func (n FullName) Format() Value { return FullNameFormatter(string(n)) }
