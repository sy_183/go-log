package log

import (
	"gitee.com/csstx2020/cvds2020/go-common/component"
	"gitee.com/csstx2020/cvds2020/go-common/lock"
	"sync"
)

var globalLogger = component.AtomicPointer[Logger]{
	Init: func() *Logger {
		logger := New(WithConsoleEncoder(), WithStdoutCore)
		return &logger
	},
}

var globalMu sync.Mutex

func GlobalLogger() Logger {
	return *globalLogger.Get()
}

func setGlobalLogger(logger Logger) {
	globalLogger.Set(&logger)
}

func SetGlobalLogger(logger Logger) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(logger) })
}

func GlobalWith(fields ...Field) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().With(fields...)) })
}

func GlobalWithCore(core Core) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithCore(core)) })
}

func GlobalWithEncoder(encoder Encoder) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithEncoder(encoder)) })
}

func GlobalWithClock(clock Clock) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithClock(clock)) })
}

func GlobalWithErrLogger(logger Logger) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithErrLogger(logger)) })
}

func GlobalWithDevelopment(development bool) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithDevelopment(development)) })
}

func GlobalWithIgnoreError(ignore bool) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithIgnoreError(ignore)) })
}

func GlobalWithAddCaller(addCaller LevelEnabler) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithAddCaller(addCaller)) })
}

func GlobalWithAddStack(addStack LevelEnabler) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithAddStack(addStack)) })
}

func GlobalWithName(name string) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithName(name)) })
}

func GlobalWithLevel(level LevelEnabler) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithLevel(level)) })
}

func GlobalWithCallerSkip(callerSkip int) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithCallerSkip(callerSkip)) })
}

func GlobalWithFields(fields ...Field) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithFields(fields...)) })
}

func GlobalWithEncoderOptions(options ...EncoderOption) {
	lock.LockDo(&globalMu, func() { setGlobalLogger(GlobalLogger().WithEncoderOptions(options...)) })
}

func Log(level LevelI, msg string, fields ...Field) {
	GlobalLogger().AddCallerSkip(1).Log(level, msg, fields...)
}

func Trace(msg string, fields ...Field) {
	GlobalLogger().AddCallerSkip(1).Trace(msg, fields...)
}

func Debug(msg string, fields ...Field) {
	GlobalLogger().AddCallerSkip(1).Debug(msg, fields...)
}

func Info(msg string, fields ...Field) {
	GlobalLogger().AddCallerSkip(1).Info(msg, fields...)
}

func Warn(msg string, fields ...Field) {
	GlobalLogger().AddCallerSkip(1).Warn(msg, fields...)
}

func Error(msg string, fields ...Field) {
	GlobalLogger().AddCallerSkip(1).Error(msg, fields...)
}

func DPanic(msg string, fields ...Field) {
	GlobalLogger().AddCallerSkip(1).DPanic(msg, fields...)
}

func Panic(msg string, fields ...Field) {
	GlobalLogger().AddCallerSkip(1).Panic(msg, fields...)
}

func Fatal(msg string, fields ...Field) {
	GlobalLogger().AddCallerSkip(1).Fatal(msg, fields...)
}

func Logf(level LevelI, format string, args ...any) {
	GlobalLogger().AddCallerSkip(1).Logf(level, format, args...)
}

func Tracef(format string, args ...any) {
	GlobalLogger().AddCallerSkip(1).Tracef(format, args...)
}

func Debugf(format string, args ...any) {
	GlobalLogger().AddCallerSkip(1).Debugf(format, args...)
}

func Infof(format string, args ...any) {
	GlobalLogger().AddCallerSkip(1).Infof(format, args...)
}

func Warnf(format string, args ...any) {
	GlobalLogger().AddCallerSkip(1).Warnf(format, args...)
}

func Errorf(format string, args ...any) {
	GlobalLogger().AddCallerSkip(1).Errorf(format, args...)
}

func DPanicf(format string, args ...any) {
	GlobalLogger().AddCallerSkip(1).DPanicf(format, args...)
}

func Panicf(format string, args ...any) {
	GlobalLogger().AddCallerSkip(1).Panicf(format, args...)
}

func Fatalf(format string, args ...any) {
	GlobalLogger().AddCallerSkip(1).Fatalf(format, args...)
}
