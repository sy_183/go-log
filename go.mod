module gitee.com/csstx2020/cvds2020/go-log

go 1.23

require (
	gitee.com/csstx2020/cvds2020/go-common v1.0.4
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/kr/text v0.2.0 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
)

replace gitee.com/csstx2020/cvds2020/go-common v1.0.4 => ../common
