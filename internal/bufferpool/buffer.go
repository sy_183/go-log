package bufferpool

import (
	"strconv"
	"time"
)

// Buffer 是对字节切片的轻量包装。它的目的是被池化使用，因此构建它的唯一方法是通过一个池
// (BufferPool)对象。
type Buffer struct {
	bs   []byte
	pool *BufferPool
}

func NewBuffer(size int) *Buffer {
	return &Buffer{bs: make([]byte, 0, size)}
}

// AppendByte 向buffer中写入一个字节
func (b *Buffer) AppendByte(v byte) {
	b.bs = append(b.bs, v)
}

// AppendString 向buffer中写入字符串
func (b *Buffer) AppendString(s string) {
	b.bs = append(b.bs, s...)
}

// AppendInt 向buffer中写入一个整数的字符串格式(使用十进制).
func (b *Buffer) AppendInt(i int64) {
	b.bs = strconv.AppendInt(b.bs, i, 10)
}

// AppendTime 向buffer中写入指定格式的时间字符串
func (b *Buffer) AppendTime(t time.Time, layout string) {
	b.bs = t.AppendFormat(b.bs, layout)
}

// AppendUint 向buffer中写入一个无符号整数的字符串格式(使用十进制).
func (b *Buffer) AppendUint(i uint64) {
	b.bs = strconv.AppendUint(b.bs, i, 10)
}

// AppendBool 向buffer中写入一个布尔值的字符串格式(true和false).
func (b *Buffer) AppendBool(v bool) {
	b.bs = strconv.AppendBool(b.bs, v)
}

// AppendFloat 向buffer中写入一个浮点数的字符串格式
func (b *Buffer) AppendFloat(f float64, bitSize int) {
	b.bs = strconv.AppendFloat(b.bs, f, 'f', -1, bitSize)
}

// Len 返回buffer缓冲区的长度
func (b *Buffer) Len() int {
	return len(b.bs)
}

// Cap 返回buffer缓冲区的容量
func (b *Buffer) Cap() int {
	return cap(b.bs)
}

// Bytes 返回buffer缓冲区的内容
func (b *Buffer) Bytes() []byte {
	return b.bs
}

// String 返回buffer缓冲区内容拷贝的字符串
func (b *Buffer) String() string {
	return string(b.bs)
}

// Reset 清空buffer缓冲区
func (b *Buffer) Reset() {
	b.bs = b.bs[:0]
}

// Write 继承 io.Writer
func (b *Buffer) Write(bs []byte) (int, error) {
	b.bs = append(b.bs, bs...)
	return len(bs), nil
}

// WriteByte 继承 io.ByteWriter
func (b *Buffer) WriteByte(v byte) error {
	b.AppendByte(v)
	return nil
}

// WriteString 继承 io.StringWriter
func (b *Buffer) WriteString(s string) (int, error) {
	b.AppendString(s)
	return len(s), nil
}

// TrimNewline 去除buffer缓冲区末尾的最后一个"\n"字符
func (b *Buffer) TrimNewline() {
	if i := len(b.bs) - 1; i >= 0 {
		if b.bs[i] == '\n' {
			b.bs = b.bs[:i]
		}
	}
}

// Free 释放buffer缓冲区
func (b *Buffer) Free() {
	if b.pool != nil {
		b.pool.put(b)
	}
}
