package log

import (
	"encoding/base64"
	"encoding/json"
	"gitee.com/csstx2020/cvds2020/go-common/component"
	"gitee.com/csstx2020/cvds2020/go-common/errors"
	"gitee.com/csstx2020/cvds2020/go-common/unsafe"
	"gitee.com/csstx2020/cvds2020/go-log/internal/bufferpool"
	"math"
	"runtime"
	"time"
	"unicode/utf8"
)

const (
	LineEndingCR   = "\r"
	LineEndingLF   = "\n"
	LineEndingCRLF = "\r\n"
)

var DefaultLineEnding = component.NewElem(func() string {
	if runtime.GOOS == "windows" {
		return LineEndingCRLF
	} else {
		return LineEndingLF
	}
})

const (
	Json2SpaceIndent  = "  "
	Json4SpaceIndent  = "    "
	JsonTabIndent     = "\t"
	JsonDefaultIndent = Json4SpaceIndent
)

// hex 与 json.hex 保持同步
var hex = "0123456789abcdef"

// safeSet 与 json.safeSet 保持同步
var safeSet = [utf8.RuneSelf]bool{
	' ':      true,
	'!':      true,
	'"':      false,
	'#':      true,
	'$':      true,
	'%':      true,
	'&':      true,
	'\'':     true,
	'(':      true,
	')':      true,
	'*':      true,
	'+':      true,
	',':      true,
	'-':      true,
	'.':      true,
	'/':      true,
	'0':      true,
	'1':      true,
	'2':      true,
	'3':      true,
	'4':      true,
	'5':      true,
	'6':      true,
	'7':      true,
	'8':      true,
	'9':      true,
	':':      true,
	';':      true,
	'<':      true,
	'=':      true,
	'>':      true,
	'?':      true,
	'@':      true,
	'A':      true,
	'B':      true,
	'C':      true,
	'D':      true,
	'E':      true,
	'F':      true,
	'G':      true,
	'H':      true,
	'I':      true,
	'J':      true,
	'K':      true,
	'L':      true,
	'M':      true,
	'N':      true,
	'O':      true,
	'P':      true,
	'Q':      true,
	'R':      true,
	'S':      true,
	'T':      true,
	'U':      true,
	'V':      true,
	'W':      true,
	'X':      true,
	'Y':      true,
	'Z':      true,
	'[':      true,
	'\\':     false,
	']':      true,
	'^':      true,
	'_':      true,
	'`':      true,
	'a':      true,
	'b':      true,
	'c':      true,
	'd':      true,
	'e':      true,
	'f':      true,
	'g':      true,
	'h':      true,
	'i':      true,
	'j':      true,
	'k':      true,
	'l':      true,
	'm':      true,
	'n':      true,
	'o':      true,
	'p':      true,
	'q':      true,
	'r':      true,
	's':      true,
	't':      true,
	'u':      true,
	'v':      true,
	'w':      true,
	'x':      true,
	'y':      true,
	'z':      true,
	'{':      true,
	'|':      true,
	'}':      true,
	'~':      true,
	'\u007f': true,
}

var safeSetWithESC = safeSet

// htmlSafeSet 与 json.htmlSafeSet 保持同步
var htmlSafeSet = [utf8.RuneSelf]bool{
	' ':      true,
	'!':      true,
	'"':      false,
	'#':      true,
	'$':      true,
	'%':      true,
	'&':      false,
	'\'':     true,
	'(':      true,
	')':      true,
	'*':      true,
	'+':      true,
	',':      true,
	'-':      true,
	'.':      true,
	'/':      true,
	'0':      true,
	'1':      true,
	'2':      true,
	'3':      true,
	'4':      true,
	'5':      true,
	'6':      true,
	'7':      true,
	'8':      true,
	'9':      true,
	':':      true,
	';':      true,
	'<':      false,
	'=':      true,
	'>':      false,
	'?':      true,
	'@':      true,
	'A':      true,
	'B':      true,
	'C':      true,
	'D':      true,
	'E':      true,
	'F':      true,
	'G':      true,
	'H':      true,
	'I':      true,
	'J':      true,
	'K':      true,
	'L':      true,
	'M':      true,
	'N':      true,
	'O':      true,
	'P':      true,
	'Q':      true,
	'R':      true,
	'S':      true,
	'T':      true,
	'U':      true,
	'V':      true,
	'W':      true,
	'X':      true,
	'Y':      true,
	'Z':      true,
	'[':      true,
	'\\':     false,
	']':      true,
	'^':      true,
	'_':      true,
	'`':      true,
	'a':      true,
	'b':      true,
	'c':      true,
	'd':      true,
	'e':      true,
	'f':      true,
	'g':      true,
	'h':      true,
	'i':      true,
	'j':      true,
	'k':      true,
	'l':      true,
	'm':      true,
	'n':      true,
	'o':      true,
	'p':      true,
	'q':      true,
	'r':      true,
	's':      true,
	't':      true,
	'u':      true,
	'v':      true,
	'w':      true,
	'x':      true,
	'y':      true,
	'z':      true,
	'{':      true,
	'|':      true,
	'}':      true,
	'~':      true,
	'\u007f': true,
}

var htmlSafeSetWithESC = htmlSafeSet

func init() {
	safeSetWithESC['\x1b'] = true
	htmlSafeSetWithESC['\x1b'] = true
}

type (
	jsonEncoderConfig struct {
		messageKey    string
		levelKey      string
		timeKey       string
		nameKey       string
		callerKey     string
		functionKey   string
		stacktraceKey string

		arrayStartSpace int
		arrayEndSpace   int
		mapStartSpace   int
		mapEndSpace     int
		keyValueSpace   int
		elemSpace       int

		skipLineEnding bool
		lineEnding     string
		indent         string
		indentPrefix   string

		escapeHTML bool
		escapeESC  bool

		levelFormatter    LevelFormatter
		timeFormatter     TimeFormatter
		durationFormatter DurationFormatter
		callerFormatter   CallerFormatter
		nameFormatter     NameFormatter

		errorHandler EncoderErrorHandler
	}
	jeCfg = jsonEncoderConfig
)

func (c *jeCfg) setMessageKey(key string)                         { c.messageKey = key }
func (c *jeCfg) setLevelKey(key string)                           { c.levelKey = key }
func (c *jeCfg) setTimeKey(key string)                            { c.timeKey = key }
func (c *jeCfg) setNameKey(key string)                            { c.nameKey = key }
func (c *jeCfg) setCallerKey(key string)                          { c.callerKey = key }
func (c *jeCfg) setFunctionKey(key string)                        { c.functionKey = key }
func (c *jeCfg) setStacktraceKey(key string)                      { c.stacktraceKey = key }
func (c *jeCfg) setArrayStartSpace(space int)                     { c.arrayStartSpace = space }
func (c *jeCfg) setArrayEndSpace(space int)                       { c.arrayEndSpace = space }
func (c *jeCfg) setMapStartSpace(space int)                       { c.mapStartSpace = space }
func (c *jeCfg) setMapEndSpace(space int)                         { c.mapEndSpace = space }
func (c *jeCfg) setKeyValueSpace(space int)                       { c.keyValueSpace = space }
func (c *jeCfg) setElemSpace(space int)                           { c.elemSpace = space }
func (c *jeCfg) setSkipLineEnding(skip bool)                      { c.skipLineEnding = skip }
func (c *jeCfg) setLineEnding(lineEnding string)                  { c.lineEnding = lineEnding }
func (c *jeCfg) setIndent(indent string)                          { c.indent = indent }
func (c *jeCfg) setIndentPrefix(prefix string)                    { c.indentPrefix = prefix }
func (c *jeCfg) setEscapeHTML(escape bool)                        { c.escapeHTML = escape }
func (c *jeCfg) setEscapeESC(escape bool)                         { c.escapeESC = escape }
func (c *jeCfg) setLevelFormatter(formatter LevelFormatter)       { c.levelFormatter = formatter }
func (c *jeCfg) setTimeFormatter(formatter TimeFormatter)         { c.timeFormatter = formatter }
func (c *jeCfg) setDurationFormatter(formatter DurationFormatter) { c.durationFormatter = formatter }
func (c *jeCfg) setCallerFormatter(formatter CallerFormatter)     { c.callerFormatter = formatter }
func (c *jeCfg) setNameFormatter(formatter NameFormatter)         { c.nameFormatter = formatter }
func (c *jeCfg) setErrorHandler(handler EncoderErrorHandler)      { c.errorHandler = handler }

type (
	jsonEncoderCtx struct {
		*jsonEncoderConfig
		sep    jsonEncoderSeparator
		depth  int
		buffer *bufferpool.Buffer
	}
	jec = jsonEncoderCtx
)

func (c *jec) addIndentPrefix() {
	if len(c.indentPrefix) > 0 {
		c.buffer.AppendString(c.indentPrefix)
	}
}

func (c *jec) newLine() {
	c.buffer.AppendString(string(c.lineEnding))
	c.addIndentPrefix()
	for i := 0; i < c.depth; i++ {
		c.buffer.AppendString(c.indent)
	}
}

func (c *jec) addSpace(space int) {
	for i := 0; i < space; i++ {
		c.buffer.AppendByte(' ')
	}
}

func (c *jec) addSpaceOrNewLine(space int) {
	if len(c.indent) > 0 {
		c.newLine()
	} else {
		c.addSpace(space)
	}
}

type jsonEncoderSeparator int

const (
	jsonEncoderSeparatorDocumentStart = jsonEncoderSeparator(iota)
	jsonEncoderSeparatorArrayStart
	jsonEncoderSeparatorMapStart
	jsonEncoderSeparatorKeyValue
	jsonEncoderSeparatorElement
	jsonEncoderSeparatorAdded
)

func (c *jec) addSeparator() {
	switch c.sep {
	case jsonEncoderSeparatorDocumentStart:
		c.addIndentPrefix()
	case jsonEncoderSeparatorArrayStart:
		c.addSpaceOrNewLine(c.arrayStartSpace)
	case jsonEncoderSeparatorMapStart:
		c.addSpaceOrNewLine(c.mapStartSpace)
	case jsonEncoderSeparatorKeyValue:
		c.buffer.AppendByte(':')
		c.addSpace(c.keyValueSpace)
	case jsonEncoderSeparatorElement:
		c.buffer.AppendByte(',')
		c.addSpaceOrNewLine(c.elemSpace)
	}
	c.sep = jsonEncoderSeparatorAdded
}

func (c *jec) openMap() {
	c.addSeparator()
	c.buffer.AppendByte('{')
	c.depth++
	c.sep = jsonEncoderSeparatorMapStart
}

func (c *jec) closeMap(empty bool) {
	c.depth--
	if !empty {
		c.addSpaceOrNewLine(c.mapEndSpace)
	}
	c.buffer.AppendByte('}')
	c.sep = jsonEncoderSeparatorElement
}

func (c *jec) openArray() {
	c.addSeparator()
	c.buffer.AppendByte('[')
	c.depth++
	c.sep = jsonEncoderSeparatorArrayStart
}

func (c *jec) closeArray(empty bool) {
	c.depth--
	if !empty {
		c.addSpaceOrNewLine(c.arrayEndSpace)
	}
	c.buffer.AppendByte(']')
	c.sep = jsonEncoderSeparatorElement
}

func (c *jec) encodeKey(key string) {
	c.encodeString(key)
	c.sep = jsonEncoderSeparatorKeyValue
}

func (c *jec) encodeNil() error {
	c.buffer.AppendString("null")
	return nil
}

func (c *jec) encodeBinary(val []byte) error {
	c.buffer.AppendByte('"')
	base64Enc := base64.NewEncoder(base64.StdEncoding, c.buffer)
	if _, err := base64Enc.Write(val); err != nil {
		return err
	}
	if err := base64Enc.Close(); err != nil {
		return err
	}
	c.buffer.AppendByte('"')
	return nil
}

func (c *jec) encodeBool(val bool) error {
	encodeBool(c.buffer, val)
	return nil
}

func (c *jec) encodeComplex(val complex128, precision int) error {
	c.buffer.AppendByte('"')
	encodeComplex(c.buffer, val, precision)
	c.buffer.AppendByte('"')
	return nil
}

func (c *jec) encodeComplex128(val complex128) error { return c.encodeComplex(val, 64) }
func (c *jec) encodeComplex64(val complex64) error   { return c.encodeComplex(complex128(val), 32) }

func (c *jec) encodeFloat(val float64, bitSize int) error {
	switch {
	case math.IsNaN(val):
		c.buffer.AppendString(`"NaN"`)
	case math.IsInf(val, 1):
		c.buffer.AppendString(`"+Inf"`)
	case math.IsInf(val, -1):
		c.buffer.AppendString(`"-Inf"`)
	default:
		c.buffer.AppendFloat(val, bitSize)
	}
	return nil
}

func (c *jec) encodeFloat64(val float64) error { return c.encodeFloat(val, 64) }
func (c *jec) encodeFloat32(val float32) error { return c.encodeFloat(float64(val), 32) }

func (c *jec) encodeInt64(val int64) error {
	c.buffer.AppendInt(val)
	return nil
}

func (c *jec) encodeInt32(val int32) error { return c.encodeInt64(int64(val)) }
func (c *jec) encodeInt16(val int16) error { return c.encodeInt64(int64(val)) }
func (c *jec) encodeInt8(val int8) error   { return c.encodeInt64(int64(val)) }

func (c *jec) encodeString(s string) error {
	c.buffer.AppendByte('"')
	start := 0
	for i := 0; i < len(s); {
		if b := s[i]; b < utf8.RuneSelf {
			if !c.escapeESC {
				if htmlSafeSetWithESC[b] || (!c.escapeHTML && safeSetWithESC[b]) {
					i++
					continue
				}
			} else {
				if htmlSafeSet[b] || (!c.escapeHTML && safeSet[b]) {
					i++
					continue
				}
			}
			if start < i {
				c.buffer.AppendString(s[start:i])
			}
			c.buffer.AppendByte('\\')
			switch b {
			case '\\', '"':
				c.buffer.AppendByte(b)
			case '\n':
				c.buffer.AppendByte('n')
			case '\r':
				c.buffer.AppendByte('r')
			case '\t':
				c.buffer.AppendByte('t')
			default:
				// This encodes bytes < 0x20 except for \t, \n and \r.
				// If escapeHTML is set, it also escapes <, >, and &
				// because they can lead to security holes when
				// user-controlled strings are rendered into JSON
				// and served to some browsers.
				c.buffer.AppendString(`u00`)
				c.buffer.AppendByte(hex[b>>4])
				c.buffer.AppendByte(hex[b&0xF])
			}
			i++
			start = i
			continue
		}
		ch, size := utf8.DecodeRuneInString(s[i:])
		if ch == utf8.RuneError && size == 1 {
			if start < i {
				c.buffer.AppendString(s[start:i])
			}
			c.buffer.AppendString(`\ufffd`)
			i += size
			start = i
			continue
		}
		// U+2028 is LINE SEPARATOR.
		// U+2029 is PARAGRAPH SEPARATOR.
		// They are both technically valid characters in JSON strings,
		// but don't work in JSONP, which has to be evaluated as JavaScript,
		// and can lead to security holes there. It is valid JSON to
		// escape them, so we do so unconditionally.
		// See http://timelessrepo.com/json-isnt-a-javascript-subset for discussion.
		if ch == '\u2028' || ch == '\u2029' {
			if start < i {
				c.buffer.AppendString(s[start:i])
			}
			c.buffer.AppendString(`\u202`)
			c.buffer.AppendByte(hex[ch&0xF])
			i += size
			start = i
			continue
		}
		i += size
	}
	if start < len(s) {
		c.buffer.AppendString(s[start:])
	}
	c.buffer.AppendByte('"')
	return nil
}

func (c *jec) encodeUint64(val uint64) error {
	c.buffer.AppendUint(val)
	return nil
}

func (c *jec) encodeUint32(val uint32) error   { return c.encodeUint64(uint64(val)) }
func (c *jec) encodeUint16(val uint16) error   { return c.encodeUint64(uint64(val)) }
func (c *jec) encodeUint8(val uint8) error     { return c.encodeUint64(uint64(val)) }
func (c *jec) encodeUintptr(val uintptr) error { return c.encodeUint64(uint64(val)) }

func (c *jec) encodeTime(val time.Time) error         { return c.encodeValue(c.FormatTime(val)) }
func (c *jec) encodeDuration(val time.Duration) error { return c.encodeValue(c.FormatDuration(val)) }

func (c *jec) encodeErrorsElements(es []error) error {
	for _, err := range es {
		c.addSeparator()
		if err := c.encodeError(err); err != nil {
			return err
		}
		c.sep = jsonEncoderSeparatorElement
	}
	return nil
}

func (c *jec) encodeErrors(es []error) error {
	c.openArray()
	if err := c.encodeErrorsElements(es); err != nil {
		return err
	}
	c.closeArray(len(es) == 0)
	return nil
}

func (c *jec) encodeError(err error) error {
	switch re := err.(type) {
	case ValueFormatter:
		return c.encodeValue(re.Format())
	case errors.ErrorGroup:
		return c.encodeErrors(re.Errors())
	default:
		return c.encodeString(re.Error())
	}
}

func (c *jec) encodeObject(val any) error {
	je := json.NewEncoder(c.buffer)
	je.SetEscapeHTML(c.escapeHTML)
	if len(c.indent) > 0 {
		prefix := c.indentPrefix
		for i := 0; i < c.depth; i++ {
			prefix += c.indent
		}
		je.SetIndent(prefix, c.indent)
	}
	err := je.Encode(val)
	if err != nil {
		return err
	}
	if len(c.indent) == 0 {
		c.buffer.TrimNewline()
	}
	return nil
}

func (c *jec) encodeValuesElements(values []Value) (n int, err error) {
	for _, value := range values {
		if value.Type == SkipType {
			continue
		}
		c.addSeparator()
		if err := c.encodeValue(value); err != nil {
			return n, err
		}
		c.sep = jsonEncoderSeparatorElement
		n++
	}
	return n, nil
}

func (c *jec) encodeValues(values []Value) error {
	c.openArray()
	n, err := c.encodeValuesElements(values)
	if err != nil {
		return err
	}
	c.closeArray(n == 0)
	return nil
}

func (c *jec) encodeFieldsElements(fields []Field) (n int, err error) {
	for _, field := range fields {
		if field.Type == SkipType {
			continue
		}
		c.addSeparator()
		c.encodeKey(field.Key)
		c.addSeparator()
		if err := c.encodeValue(field.Value); err != nil {
			return n, err
		}
		c.sep = jsonEncoderSeparatorElement
		n++
	}
	return n, nil
}

func (c *jec) encodeFields(fields []Field) error {
	c.openMap()
	n, err := c.encodeFieldsElements(fields)
	if err != nil {
		return err
	}
	c.closeMap(n == 0)
	return nil
}

func (c *jec) encodeSkipV(value Value) error       { return nil }
func (c *jec) encodeNilV(value Value) error        { return c.encodeNil() }
func (c *jec) encodeBinaryV(value Value) error     { return c.encodeBinary(value.Binary()) }
func (c *jec) encodeBoolV(value Value) error       { return c.encodeBool(value.Bool()) }
func (c *jec) encodeComplex128V(value Value) error { return c.encodeComplex128(value.Complex128()) }
func (c *jec) encodeComplex64V(value Value) error  { return c.encodeComplex64(value.Complex64()) }
func (c *jec) encodeFloat64V(value Value) error    { return c.encodeFloat64(value.Float64()) }
func (c *jec) encodeFloat32V(value Value) error    { return c.encodeFloat32(value.Float32()) }
func (c *jec) encodeInt64V(value Value) error      { return c.encodeInt64(value.Int64()) }
func (c *jec) encodeInt32V(value Value) error      { return c.encodeInt32(value.Int32()) }
func (c *jec) encodeInt16V(value Value) error      { return c.encodeInt16(value.Int16()) }
func (c *jec) encodeInt8V(value Value) error       { return c.encodeInt8(value.Int8()) }
func (c *jec) encodeStringV(value Value) error     { return c.encodeString(value.String()) }
func (c *jec) encodeUint64V(value Value) error     { return c.encodeUint64(value.Uint64()) }
func (c *jec) encodeUint32V(value Value) error     { return c.encodeUint32(value.Uint32()) }
func (c *jec) encodeUint16V(value Value) error     { return c.encodeUint16(value.Uint16()) }
func (c *jec) encodeUint8V(value Value) error      { return c.encodeUint8(value.Uint8()) }
func (c *jec) encodeUintptrV(value Value) error    { return c.encodeUintptr(value.Uintptr()) }
func (c *jec) encodeTimeV(value Value) error       { return c.encodeTime(value.Time()) }
func (c *jec) encodeTimeFullV(value Value) error   { return c.encodeTime(value.TimeFull()) }
func (c *jec) encodeDurationV(value Value) error   { return c.encodeDuration(value.Duration()) }
func (c *jec) encodeStringerV(value Value) error   { return c.encodeString(value.Stringer().String()) }
func (c *jec) encodeFormatterV(value Value) error  { return c.encodeValue(value.Formatter().Format()) }
func (c *jec) encodeErrorV(value Value) error      { return c.encodeError(value.Error()) }
func (c *jec) encodeObjectV(value Value) error     { return c.encodeObject(value.Object()) }
func (c *jec) encodeValuesV(value Value) error     { return c.encodeValues(value.Values()) }
func (c *jec) encodeFieldsV(value Value) error     { return c.encodeFields(value.Fields()) }

func (c *jec) encodeValue(value Value) error {
	if value.Type >= MaxTypes {
		return InvalidValueTypeError{Type: value.Type}
	}
	if encoder := jsonEncoderMap[value.Type]; encoder != nil {
		return encoder(c, value)
	}
	return InvalidValueTypeError{Type: value.Type}
}

func (c *jec) FormatLevel(l LevelI) Value {
	formatter := c.levelFormatter
	if formatter == nil {
		formatter = LowercaseLevelFormatter
	}
	return formatter(l)
}

func (c *jec) FormatTime(t time.Time) Value {
	formatter := c.timeFormatter
	if formatter == nil {
		formatter = EpochTimeFormatter
	}
	return formatter(t)
}

func (c *jec) FormatDuration(duration time.Duration) Value {
	formatter := c.durationFormatter
	if formatter == nil {
		formatter = SecondDurationFormatter
	}
	return formatter(duration)
}

func (c *jec) FormatCaller(caller EntryCaller) Value {
	formatter := c.callerFormatter
	if formatter == nil {
		formatter = FullCallerFormatter
	}
	return formatter(caller)
}

func (c *jec) FormatName(name string) Value {
	formatter := c.nameFormatter
	if formatter == nil {
		formatter = FullNameFormatter
	}
	return formatter(name)
}

var jsonEncoderMap [MaxTypes]func(*jec, Value) error

func init() {
	jsonEncoderMap = [MaxTypes]func(*jec, Value) error{
		SkipType:       (*jec).encodeSkipV,
		NilType:        (*jec).encodeNilV,
		BinaryType:     (*jec).encodeBinaryV,
		BoolType:       (*jec).encodeBoolV,
		Complex128Type: (*jec).encodeComplex128V,
		Complex64Type:  (*jec).encodeComplex64V,
		Float64Type:    (*jec).encodeFloat64V,
		Float32Type:    (*jec).encodeFloat32V,
		Int64Type:      (*jec).encodeInt64V,
		Int32Type:      (*jec).encodeInt32V,
		Int16Type:      (*jec).encodeInt16V,
		Int8Type:       (*jec).encodeInt8V,
		StringType:     (*jec).encodeStringV,
		Uint64Type:     (*jec).encodeUint64V,
		Uint32Type:     (*jec).encodeUint32V,
		Uint16Type:     (*jec).encodeUint16V,
		Uint8Type:      (*jec).encodeUint8V,
		UintptrType:    (*jec).encodeUintptrV,
		TimeType:       (*jec).encodeTimeV,
		TimeFullType:   (*jec).encodeTimeFullV,
		DurationType:   (*jec).encodeDurationV,
		StringerType:   (*jec).encodeStringerV,
		FormatterType:  (*jec).encodeFormatterV,
		ErrorType:      (*jec).encodeErrorV,
		ObjectType:     (*jec).encodeObjectV,
		SliceType:      (*jec).encodeObjectV,
		MapType:        (*jec).encodeObjectV,
		ValuesType:     (*jec).encodeValuesV,
		FieldsType:     (*jec).encodeFieldsV,
	}
}

func (c *jec) encode(entry *Entry, fields []Field) (res any, err error) {
	c.openMap()

	var entryFields []Field

	if c.levelKey != "" {
		entryFields = append(entryFields, field(c.levelKey, c.FormatLevel(entry.Level)))
	}

	if c.timeKey != "" {
		entryFields = append(entryFields, field(c.timeKey, c.FormatTime(entry.Time)))
	}

	if entry.LoggerName != "" && c.nameKey != "" {
		entryFields = append(entryFields, String(c.nameKey, entry.LoggerName))
	}

	if entry.Caller.Defined {
		if c.callerKey != "" {
			entryFields = append(entryFields, field(c.callerKey, c.FormatCaller(entry.Caller)))
		}
		if c.functionKey != "" {
			entryFields = append(entryFields, String(c.functionKey, entry.Caller.Function))
		}
	}

	if c.messageKey != "" && entry.Message != "" {
		entryFields = append(entryFields, String(c.messageKey, entry.Message))
	}

	var n1, n2, n3, n4 int
	if n1, err = c.encodeFieldsElements(entryFields); err != nil {
		return nil, err
	}
	if n2, err = c.encodeFieldsElements(entry.Fields); err != nil {
		return nil, err
	}
	if n3, err = c.encodeFieldsElements(fields); err != nil {
		return nil, err
	}

	if entry.Stack != "" && c.stacktraceKey != "" {
		entryFields = entryFields[:0]
		entryFields = append(entryFields, String(c.stacktraceKey, entry.Stack))
		if n4, err = c.encodeFieldsElements(entryFields); err != nil {
			return nil, err
		}
	}

	c.closeMap(n1+n2+n3+n4 == 0)
	if !c.skipLineEnding {
		c.buffer.AppendString(c.lineEnding)
	}

	return c.buffer, nil
}

type jsonEncoder struct {
	*jsonEncoderConfig
}

func newJsonEncoder(options ...EncoderOption) jsonEncoder {
	enc := jsonEncoder{
		jsonEncoderConfig: &jsonEncoderConfig{
			messageKey:    "msg",
			levelKey:      "level",
			timeKey:       "time",
			nameKey:       "name",
			callerKey:     "caller",
			functionKey:   "func",
			stacktraceKey: "stack",
			keyValueSpace: 1,
			elemSpace:     1,
			lineEnding:    DefaultLineEnding.Get(),
			escapeHTML:    false,
			errorHandler:  DefaultEncoderErrorHandler,
		},
	}
	for _, option := range options {
		option.Apply(enc)
	}
	return enc
}

func NewJsonEncoder(options ...EncoderOption) Encoder {
	return newJsonEncoder(options...)
}

func (enc jsonEncoder) clone() jsonEncoder {
	return jsonEncoder{jsonEncoderConfig: clone(enc.jsonEncoderConfig)}
}

func (enc jsonEncoder) WithOptions(options ...EncoderOption) Encoder {
	n := enc.clone()
	for _, option := range options {
		option.Apply(n)
	}
	return n
}

func (enc jsonEncoder) Encode(logger Logger, entry *Entry, fields ...Field) (any, error) {
	ctx := unsafe.Unescape(&jsonEncoderCtx{
		jsonEncoderConfig: enc.jsonEncoderConfig,
		buffer:            bufferpool.Get(),
	})
	res, err := ctx.encode(entry, fields)
	if err != nil {
		if enc.errorHandler != nil {
			enc.errorHandler(enc, logger, err)
		}
		ctx.buffer.Free()
		return nil, err
	}
	return res, nil
}
