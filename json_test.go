package log

import (
	"encoding/json"
	"strings"
	"testing"
	"time"
)

func TestJson(t *testing.T) {
	sb := strings.Builder{}
	enc := json.NewEncoder(&sb)
	if err := enc.Encode(struct {
		A string
		B int64
		C float64
		D struct {
			A uint16
			B []byte
			C uintptr
			D struct {
				A time.Time
				B time.Duration
				C []int
			}
		}
		M map[string]string
	}{
		A: "<script>",
		M: map[string]string{
			"<script>": "<script>",
		},
	}); err != nil {
		panic(err)
	}
	print(sb.String())
}
