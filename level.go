package log

import (
	"bytes"
	"encoding"
	"fmt"
	"gitee.com/csstx2020/cvds2020/go-common/sgr"
	"math"
)

// LevelEnabler 接口用于判断指定日志等级的某些信息是否启用
type LevelEnabler interface {
	Enabled(LevelI) bool
}

// LevelEnablerFunc 为 LevelEnabler 的函数式实现
type LevelEnablerFunc func(LevelI) bool

func (f LevelEnablerFunc) Enabled(l LevelI) bool { return f(l) }

// staticLevelEnabler 为 LevelEnabler 的静态实现
type staticLevelEnabler bool

const (
	// LevelAlwaysEnable 表示启用所有等级
	LevelAlwaysEnable staticLevelEnabler = true
	// LevelAlwaysDisable 表示禁用所有等级
	LevelAlwaysDisable staticLevelEnabler = false
)

func (e staticLevelEnabler) Enabled(LevelI) bool { return bool(e) }

// LevelI 日志等级接口，规定了日志等级的格式信息和等级大小
type LevelI interface {
	// String 返回日志等级的小写字符串格式
	String() string
	// CapitalString 返回日志等级的大写字符串格式
	CapitalString() string
	// ColorString 返回日志等级的小写字符串格式(带控制台输出颜色)
	ColorString() string
	// CapitalColorString 返回日志等级的大写字符串格式(带控制台输出颜色)
	CapitalColorString() string
	// Level 返回具体的日志等级，用于比较日志等级的大小
	Level() Level
	// 继承 encoding.TextMarshaler，用于解析字符串格式的日志等级
	encoding.TextMarshaler
}

// Level 默认的日志等级，使用整形表示用于比较日志等级的大小
type Level int

const (
	// TraceLevel 日志等级通常用于追踪某些事件的具体内容，日志量可能会很大，一般只在开发或调试
	// 时使用
	TraceLevel Level = -(1 << (13 - iota))
	// DebugLevel 日志等级通常用于开发过中细粒度的信息事件，用于开发过程中打印一些运行信息
	DebugLevel
	// InfoLevel 日志等级通常用于粗粒度级别上突出强调的信息事件，用于打印一些感兴趣或重要的信
	// 息。可用于生产环境中，但不能滥用，避免打印过多的日志
	InfoLevel Level = 0
	// WarnLevel 日志等级通常表明潜在的错误情形，有些信息不是错误信息，但也会给开发或维护人员
	// 一些提示
	WarnLevel Level = 1 << (12 + (iota - 2))
	// ErrorLevel 日志等级用于输出程序运行中的错误信息，但不影响程序继续运行
	ErrorLevel
	// DPanicLevel 日志等级用于输出程序运行中的严重错误信息，并在开启生产环境标志的日志输出器
	// 中会在输出完信息后抛出panic错误
	DPanicLevel
	// PanicLevel 日志等级用于输出程序运行中的严重错误信息，并在输出完信息后抛出panic错误
	PanicLevel
	// FatalLevel 日志等级用于输出程序运行中的致命错误信息，并在输出完信息后退出程序
	FatalLevel

	minLevel Level = math.MinInt
	maxLevel Level = math.MaxInt
)

var (
	// 日志等级对应的控制台输出颜色
	levelColor = map[Level]string{
		TraceLevel:  sgr.FgBrightBlackCSI,
		DebugLevel:  sgr.FgMagentaCSI,
		InfoLevel:   sgr.FgBlueCSI,
		WarnLevel:   sgr.FgYellowCSI,
		ErrorLevel:  sgr.FgRedCSI,
		DPanicLevel: sgr.FgRedCSI,
		PanicLevel:  sgr.FgRedCSI,
		FatalLevel:  sgr.FgBrightRedCSI,
	}
	unknownLevelColor = sgr.DefaultFgColorCSI

	levelLowercaseColorString = make(map[Level]string, len(levelColor))
	levelCapitalColorString   = make(map[Level]string, len(levelColor))
)

func init() {
	for level, color := range levelColor {
		levelLowercaseColorString[level] = sgr.WrapCSI(level.String(), color)
		levelCapitalColorString[level] = sgr.WrapCSI(level.CapitalString(), color)
	}
}

func (l Level) String() string {
	switch l {
	case TraceLevel:
		return "trace"
	case DebugLevel:
		return "debug"
	case InfoLevel:
		return "info"
	case WarnLevel:
		return "warn"
	case ErrorLevel:
		return "error"
	case DPanicLevel:
		return "dpanic"
	case PanicLevel:
		return "panic"
	case FatalLevel:
		return "fatal"
	default:
		return fmt.Sprintf("Level(%d)", l)
	}
}

func (l Level) CapitalString() string {
	switch l {
	case TraceLevel:
		return "TRACE"
	case DebugLevel:
		return "DEBUG"
	case InfoLevel:
		return "INFO"
	case WarnLevel:
		return "WARN"
	case ErrorLevel:
		return "ERROR"
	case DPanicLevel:
		return "DPANIC"
	case PanicLevel:
		return "PANIC"
	case FatalLevel:
		return "FATAL"
	default:
		return fmt.Sprintf("LEVEL(%d)", l)
	}
}

func (l Level) ColorString() string {
	s, ok := levelCapitalColorString[l]
	if !ok {
		s = sgr.WrapCSI(l.String(), unknownLevelColor)
	}
	return s
}

func (l Level) CapitalColorString() string {
	s, ok := levelCapitalColorString[l]
	if !ok {
		s = sgr.WrapCSI(l.CapitalString(), unknownLevelColor)
	}
	return s
}

func (l Level) Level() Level {
	return l
}

func (l Level) Enabled(lvl LevelI) bool {
	return lvl.Level() >= l
}

func (l *Level) Set(s string) error {
	return l.UnmarshalText([]byte(s))
}

func (l *Level) Get() interface{} {
	return *l
}

func (l Level) MarshalText() ([]byte, error) {
	return []byte(l.String()), nil
}

func (l *Level) UnmarshalText(text []byte) error {
	if l == nil {
		return NewUnmarshalNilError(l)
	}
	if !l.unmarshalText(text) && !l.unmarshalText(bytes.ToLower(text)) {
		return fmt.Errorf("unrecognized level: %q", text)
	}
	return nil
}

func (l *Level) unmarshalText(text []byte) bool {
	switch string(text) {
	case "trace", "TRACE":
		*l = TraceLevel
	case "debug", "DEBUG":
		*l = DebugLevel
	case "info", "INFO", "":
		*l = InfoLevel
	case "warn", "WARN":
		*l = WarnLevel
	case "error", "ERROR":
		*l = ErrorLevel
	case "dpanic", "DPANIC":
		*l = DPanicLevel
	case "panic", "PANIC":
		*l = PanicLevel
	case "fatal", "FATAL":
		*l = FatalLevel
	default:
		return false
	}
	return true
}
