package log

import (
	"gitee.com/csstx2020/cvds2020/go-common/pool"
	"gitee.com/csstx2020/cvds2020/go-common/unsafe"
	"gitee.com/csstx2020/cvds2020/go-log/internal/bufferpool"
	"gitee.com/csstx2020/cvds2020/go-log/internal/exit"
)

type coreLogger struct {
	core    Core
	encoder Encoder
	clock   Clock

	errLogger Logger

	development bool
	ignoreError bool
	addCaller   LevelEnabler
	addStack    LevelEnabler

	name   string
	level  LevelEnabler
	fields []Field
}

func (l *coreLogger) appendFields(fields ...Field) {
	if len(fields) > 0 {
		l.fields = append(l.fields[:len(l.fields):len(l.fields)], fields...)
	}
}

func (l *coreLogger) setCore(core Core)                   { l.core = core }
func (l *coreLogger) setEncoder(encoder Encoder)          { l.encoder = encoder }
func (l *coreLogger) setClock(clock Clock)                { l.clock = clock }
func (l *coreLogger) setErrLogger(logger Logger)          { l.errLogger = logger.WithIgnoreError(true) }
func (l *coreLogger) setDevelopment(development bool)     { l.development = development }
func (l *coreLogger) setIgnoreError(ignore bool)          { l.ignoreError = ignore }
func (l *coreLogger) setAddCaller(addCaller LevelEnabler) { l.addCaller = addCaller }
func (l *coreLogger) setAddStack(addStack LevelEnabler)   { l.addStack = addStack }
func (l *coreLogger) setName(name string)                 { l.name = name }
func (l *coreLogger) setLevel(level LevelEnabler)         { l.level = level }
func (l *coreLogger) setFields(fields ...Field)           { l.fields = fields }

func (l *coreLogger) Core() Core              { return l.core }
func (l *coreLogger) Encoder() Encoder        { return l.encoder }
func (l *coreLogger) Clock() Clock            { return l.clock }
func (l *coreLogger) ErrLogger() Logger       { return l.errLogger }
func (l *coreLogger) Development() bool       { return l.development }
func (l *coreLogger) IgnoreError() bool       { return l.ignoreError }
func (l *coreLogger) AddCaller() LevelEnabler { return l.addCaller }
func (l *coreLogger) AddStack() LevelEnabler  { return l.addStack }
func (l *coreLogger) Name() string            { return l.name }
func (l *coreLogger) Level() LevelEnabler     { return l.level }
func (l *coreLogger) Fields() []Field         { return l.fields }

func (l *coreLogger) with(fields ...Field) *coreLogger {
	n := clone(l)
	n.appendFields(fields...)
	return n
}

func (l *coreLogger) withCore(core Core) *coreLogger {
	n := clone(l)
	n.setCore(core)
	return n
}

func (l *coreLogger) withEncoder(encoder Encoder) *coreLogger {
	n := clone(l)
	n.setEncoder(encoder)
	return n
}

func (l *coreLogger) withClock(clock Clock) *coreLogger {
	n := clone(l)
	n.setClock(clock)
	return n
}

func (l *coreLogger) withErrLogger(logger Logger) *coreLogger {
	n := clone(l)
	n.setErrLogger(logger)
	return n
}

func (l *coreLogger) withDevelopment(development bool) *coreLogger {
	n := clone(l)
	n.setDevelopment(development)
	return n
}

func (l *coreLogger) withIgnoreError(ignore bool) *coreLogger {
	n := clone(l)
	n.setIgnoreError(ignore)
	return n
}

func (l *coreLogger) withAddCaller(addCaller LevelEnabler) *coreLogger {
	n := clone(l)
	n.setAddCaller(addCaller)
	return n
}

func (l *coreLogger) withAddStack(addStack LevelEnabler) *coreLogger {
	n := clone(l)
	n.setAddStack(addStack)
	return n
}

func (l *coreLogger) withName(name string) *coreLogger {
	n := clone(l)
	n.setName(name)
	return n
}

func (l *coreLogger) withLevel(level LevelEnabler) *coreLogger {
	n := clone(l)
	n.setLevel(level)
	return n
}

func (l *coreLogger) withFields(fields ...Field) *coreLogger {
	n := clone(l)
	n.setFields(fields...)
	return n
}

func (l *coreLogger) withEncoderOptions(options ...EncoderOption) *coreLogger {
	return l.withEncoder(l.encoder.WithOptions(options...))
}

func (l *coreLogger) log(skip int, level LevelI, msg string, fields []Field) {
	skip++

	lvl := level.Level()
	if lvl < DPanicLevel && !l.level.Enabled(level) {
		return
	}

	ent := Entry{
		LoggerName: l.name,
		Time:       l.clock.Now(),
		Level:      level,
		Message:    msg,
		Fields:     l.fields,
	}

	addStack := l.addStack.Enabled(level)
	addCaller := l.addCaller.Enabled(level)

	if addStack || addCaller {
		stackDepth := stacktraceFirst
		if addStack {
			stackDepth = stacktraceFull
		}
		stack := captureStacktrace(skip, stackDepth)
		defer stack.Free()

		if stack.Count() != 0 {
			frame, more := stack.Next()

			if addCaller {
				ent.Caller = EntryCaller{
					Defined:  frame.PC != 0,
					PC:       frame.PC,
					File:     frame.File,
					Line:     frame.Line,
					Function: frame.Function,
				}
			}

			if addStack {
				buffer := bufferpool.Get()
				defer buffer.Free()

				stackFmt := newStackFormatter(buffer)
				stackFmt.FormatFrame(frame)
				if more {
					stackFmt.FormatStack(stack)
				}
				ent.Stack = buffer.String()
			}
		}
	}

	pe := unsafe.Unescape(&ent)

	var encoded any
	var err error
	var errLogger Logger
	if !l.ignoreError {
		errLogger = l.errLogger
	}
	if l.encoder != nil {
		encoded, err = l.encoder.Encode(errLogger, pe, fields...)
	}

	if err == nil {
		l.core.Write(errLogger, pe, fields, encoded)
	}

	switch {
	case lvl < DPanicLevel:
	case lvl >= DPanicLevel && lvl < PanicLevel:
		if l.development {
			panic(msg)
		}
	case lvl < FatalLevel:
		panic(msg)
	default:
		exit.Exit()
	}
}

type Logger struct {
	*coreLogger
	callerSkip int
}

const (
	DefaultLevel     = InfoLevel
	DefaultAddCaller = ErrorLevel
	DefaultAddStack  = DPanicLevel
)

var DefaultErrLogger = Logger{
	coreLogger: &coreLogger{
		core:        StderrCore,
		encoder:     NewConsoleEncoder(),
		clock:       DefaultClock,
		ignoreError: true,
		addCaller:   DefaultAddCaller,
		addStack:    DefaultAddStack,
		level:       DefaultLevel,
	},
}

func newLogger(coreLogger *coreLogger, callerSkip int) Logger {
	return Logger{coreLogger: coreLogger, callerSkip: callerSkip}
}

func New(options ...Option) Logger {
	logger := Logger{
		coreLogger: &coreLogger{
			core:      NopCore,
			clock:     DefaultClock,
			addCaller: DefaultAddCaller,
			errLogger: DefaultErrLogger,
			addStack:  DefaultAddStack,
			level:     DefaultLevel,
		},
	}
	lp := unsafe.Unescape(&logger)
	for _, option := range options {
		option.Apply(lp)
	}
	return logger
}

var Nop = Logger{}

func (l Logger) IsNop() bool {
	return l.coreLogger == nil
}

func (l Logger) Core() Core {
	if l.coreLogger != nil {
		return l.coreLogger.Core()
	}
	return nil
}

func (l Logger) Encoder() Encoder {
	if l.coreLogger != nil {
		return l.coreLogger.Encoder()
	}
	return nil
}

func (l Logger) Clock() Clock {
	if l.coreLogger != nil {
		return l.coreLogger.Clock()
	}
	return nil
}

func (l Logger) ErrLogger() Logger {
	if l.coreLogger != nil {
		return l.coreLogger.ErrLogger()
	}
	return Nop
}

func (l Logger) Development() bool {
	if l.coreLogger != nil {
		return l.coreLogger.Development()
	}
	return false
}

func (l Logger) IgnoreError() bool {
	if l.coreLogger != nil {
		return l.coreLogger.IgnoreError()
	}
	return false
}

func (l Logger) AddCaller() LevelEnabler {
	if l.coreLogger != nil {
		return l.coreLogger.AddCaller()
	}
	return nil
}

func (l Logger) AddStack() LevelEnabler {
	if l.coreLogger != nil {
		return l.coreLogger.AddStack()
	}
	return nil
}

func (l Logger) Name() string {
	if l.coreLogger != nil {
		return l.coreLogger.Name()
	}
	return ""
}

func (l Logger) Level() LevelEnabler {
	if l.coreLogger != nil {
		return l.coreLogger.Level()
	}
	return nil
}

func (l Logger) Fields() []Field {
	if l.coreLogger != nil {
		return l.coreLogger.Fields()
	}
	return nil
}

func (l Logger) setCallerSkip(callerSkip int) { l.callerSkip = callerSkip }
func (l Logger) CallerSkip() int              { return l.callerSkip }

func (l Logger) With(fields ...Field) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.with(fields...), l.callerSkip)
}

func (l Logger) WithCore(core Core) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withCore(core), l.callerSkip)
}

func (l Logger) WithEncoder(encoder Encoder) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withEncoder(encoder), l.callerSkip)
}

func (l Logger) WithClock(clock Clock) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withClock(clock), l.callerSkip)
}

func (l Logger) WithErrLogger(logger Logger) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withErrLogger(logger), l.callerSkip)
}

func (l Logger) WithDevelopment(development bool) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withDevelopment(development), l.callerSkip)
}

func (l Logger) WithIgnoreError(ignore bool) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withIgnoreError(ignore), l.callerSkip)
}

func (l Logger) WithAddCaller(addCaller LevelEnabler) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withAddCaller(addCaller), l.callerSkip)
}

func (l Logger) WithAddStack(addStack LevelEnabler) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withAddStack(addStack), l.callerSkip)
}

func (l Logger) WithName(name string) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withName(name), l.callerSkip)
}

func (l Logger) WithLevel(level LevelEnabler) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withLevel(level), l.callerSkip)
}

func (l Logger) WithFields(fields ...Field) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.with(fields...), l.callerSkip)
}

func (l Logger) WithCallerSkip(skip int) Logger {
	return newLogger(l.coreLogger, skip)
}

func (l Logger) AddCallerSkip(skip int) Logger {
	return newLogger(l.coreLogger, l.callerSkip+skip)
}

func (l Logger) WithEncoderOptions(options ...EncoderOption) Logger {
	if l.IsNop() {
		return l
	}
	return newLogger(l.withEncoderOptions(options...), l.callerSkip)
}

func (l Logger) Log(level LevelI, msg string, fields ...Field) {
	if !l.IsNop() {
		l.log(l.callerSkip+1, level, msg, *unsafe.Unescape(&fields))
	}
}

func (l Logger) Trace(msg string, fields ...Field) {
	if !l.IsNop() {
		l.log(l.callerSkip+1, TraceLevel, msg, *unsafe.Unescape(&fields))
	}
}

func (l Logger) Debug(msg string, fields ...Field) {
	if !l.IsNop() {
		l.log(l.callerSkip+1, DebugLevel, msg, *unsafe.Unescape(&fields))
	}
}

func (l Logger) Info(msg string, fields ...Field) {
	if !l.IsNop() {
		l.log(l.callerSkip+1, InfoLevel, msg, *unsafe.Unescape(&fields))
	}
}

func (l Logger) Warn(msg string, fields ...Field) {
	if !l.IsNop() {
		l.log(l.callerSkip+1, WarnLevel, msg, *unsafe.Unescape(&fields))
	}
}

func (l Logger) Error(msg string, fields ...Field) {
	if !l.IsNop() {
		l.log(l.callerSkip+1, ErrorLevel, msg, *unsafe.Unescape(&fields))
	}
}

func (l Logger) DPanic(msg string, fields ...Field) {
	if !l.IsNop() {
		l.log(l.callerSkip+1, DPanicLevel, msg, *unsafe.Unescape(&fields))
	}
}

func (l Logger) Panic(msg string, fields ...Field) {
	if !l.IsNop() {
		l.log(l.callerSkip+1, PanicLevel, msg, *unsafe.Unescape(&fields))
	}
}

func (l Logger) Fatal(msg string, fields ...Field) {
	if !l.IsNop() {
		l.log(l.callerSkip+1, FatalLevel, msg, *unsafe.Unescape(&fields))
	}
}

func (l Logger) Logf(level LevelI, format string, args ...any) {
	if !l.IsNop() {
		msg, fields := formatArgs(format, *unsafe.Unescape(&args))
		l.log(l.callerSkip+1, level, msg, fields)
	}
}

func (l Logger) Tracef(format string, args ...any) {
	if !l.IsNop() {
		msg, fields := formatArgs(format, *unsafe.Unescape(&args))
		l.log(l.callerSkip+1, InfoLevel, msg, fields)
	}
}

func (l Logger) Debugf(format string, args ...any) {
	if !l.IsNop() {
		msg, fields := formatArgs(format, *unsafe.Unescape(&args))
		l.log(l.callerSkip+1, DebugLevel, msg, fields)
	}
}

func (l Logger) Infof(format string, args ...any) {
	if !l.IsNop() {
		msg, fields := formatArgs(format, *unsafe.Unescape(&args))
		l.log(l.callerSkip+1, InfoLevel, msg, fields)
	}
}

func (l Logger) Warnf(format string, args ...any) {
	if !l.IsNop() {
		msg, fields := formatArgs(format, *unsafe.Unescape(&args))
		l.log(l.callerSkip+1, WarnLevel, msg, fields)
	}
}

func (l Logger) Errorf(format string, args ...any) {
	if !l.IsNop() {
		msg, fields := formatArgs(format, *unsafe.Unescape(&args))
		l.log(l.callerSkip+1, ErrorLevel, msg, fields)
	}
}

func (l Logger) DPanicf(format string, args ...any) {
	if !l.IsNop() {
		msg, fields := formatArgs(format, *unsafe.Unescape(&args))
		l.log(l.callerSkip+1, DPanicLevel, msg, fields)
	}
}

func (l Logger) Panicf(format string, args ...any) {
	if !l.IsNop() {
		msg, fields := formatArgs(format, *unsafe.Unescape(&args))
		l.log(l.callerSkip+1, PanicLevel, msg, fields)
	}
}

func (l Logger) Fatalf(format string, args ...any) {
	if !l.IsNop() {
		msg, fields := formatArgs(format, *unsafe.Unescape(&args))
		l.log(l.callerSkip+1, FatalLevel, msg, fields)
	}
}

func (l Logger) Logger() Logger {
	return l
}

func (l Logger) Sync() error {
	if l.coreLogger != nil && l.core != nil {
		return l.core.Sync()
	}
	return nil
}

func (l Logger) Use() Logger {
	if l.coreLogger != nil && l.core != nil {
		if refCore, ok := l.core.(pool.Reference); ok {
			refCore.AddRef()
		}
	}
	return l
}

func (l Logger) Close() error {
	if l.coreLogger != nil && l.core != nil {
		return l.core.Close()
	}
	return nil
}
