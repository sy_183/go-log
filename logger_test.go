package log

import (
	"testing"
	"time"
)

func TestLogger(t *testing.T) {
	logger := New(
		WithEnableAllLevel,
		WithEncoder(NewConsoleEncoder(WithCapitalColorLevelFormatter, WithJsonDefaultIndent)),
		WithStdoutCore,
		WithName("test"))
	logger.Trace("test logger", String("key1", "string value 1"),
		Object("complex", 4+3i),
		Values("values1", StringV("value1"), IntV(0), Float64V(3.14)),
		Fields("fields1",
			String("key1", "string value 1"),
			Complex128("key2", 4+3i),
			Duration("key3", time.Second)))
	logger.Debug("test logger", String("key1", "string value 1"),
		Values("values1", StringV("value1"), IntV(0), Float64V(3.14)),
		Fields("fields1",
			String("key1", "string value 1"),
			Complex128("key2", 4+3i),
			Duration("key3", time.Second)))
	logger.Info("test logger", String("key1", "string value 1"),
		Values("values1", StringV("value1"), IntV(0), Float64V(3.14)),
		Fields("fields1",
			String("key1", "string value 1"),
			Complex128("key2", 4+3i),
			Duration("key3", time.Second)))
	logger.Warn("test logger", String("key1", "string value 1"),
		Values("values1", StringV("value1"), IntV(0), Float64V(3.14)),
		Fields("fields1",
			String("key1", "string value 1"),
			Complex128("key2", 4+3i),
			Duration("key3", time.Second)))
	logger.Error("test logger", String("key1", "string value 1"),
		Values("values1", StringV("value1"), IntV(0), Float64V(3.14)),
		Fields("fields1",
			String("key1", "string value 1"),
			Complex128("key2", 4+3i),
			Duration("key3", time.Second)))
	func() {
		defer func() { recover() }()
		logger.DPanic("test logger", String("key1", "string value 1"),
			Values("values1", StringV("value1"), IntV(0), Float64V(3.14)),
			Fields("fields1",
				String("key1", "string value 1"),
				Complex128("key2", 4+3i),
				Duration("key3", time.Second)))
	}()
	func() {
		defer func() { recover() }()
		logger.Panic("test logger", String("key1", "string value 1"),
			Values("values1", StringV("value1"), IntV(0), Float64V(3.14)),
			Fields("fields1",
				String("key1", "string value 1"),
				Complex128("key2", 4+3i),
				Duration("key3", time.Second)))
	}()
	logger.Fatal("test logger", String("key1", "string value 1"),
		Values("values1", StringV("value1"), IntV(0), Float64V(3.14)),
		Fields("fields1",
			String("key1", "string value 1"),
			Complex128("key2", 4+3i),
			Duration("key3", time.Second)))
}
