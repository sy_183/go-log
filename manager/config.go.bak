package manager

import (
	"gitee.com/sy_183/go-log"
	"gopkg.in/yaml.v3"
)

type LoggerConfig struct {
	Core        CoreConfigOrRef    `yaml:"core"`
	Encoder     EncoderConfigOrRef `yaml:"encoder"`
	Development bool               `yaml:"development"`
	IgnoreError bool               `yaml:"ignore-error"`
	AddCaller   log.Level          `yaml:"add-caller"`
	AddStack    log.Level          `yaml:"add-stack"`
	Name        string             `yaml:"name"`
	Level       log.Level          `yaml:"level"`
}

type LoggerConfigs struct {
	Configs      map[string]*LoggerConfig `yaml:",inline"`
	matchEntries []MatchEntry
}

func (c *LoggerConfigs) UnmarshalYAML(value *yaml.Node) error {
	if c == nil {
		return log.NewUnmarshalNilError(c)
	}
	type loggerConfigs LoggerConfigs
	if err := value.Decode((*loggerConfigs)(c)); err != nil {
		return err
	}
	for pattern := range c.Configs {
		entry, err := NewMatcherEntry(pattern)
		if err != nil {
			return err
		}
		c.matchEntries = append(c.matchEntries, entry)
	}
	return nil
}

type Config struct {
	Cores        map[string]*CoreConfig    `yaml:"cores"`
	Encoders     map[string]*EncoderConfig `yaml:"encoders"`
	LoggerConfig `yaml:",inline"`
	Configs      LoggerConfigs `yaml:",inline"`
}

func (c *Config) Default() *LoggerConfig {
	return &c.LoggerConfig
}

func (c *Config) Loggers() map[string]*LoggerConfig {
	return c.Configs.Configs
}

func (c *Config) GetCore(core string) *CoreConfig {
	return c.Cores[core]
}

func (c *Config) AddCore(core string, typ CoreProvider) {
	if _, exist := c.Cores[core]; exist {
		return
	}
	if c.Cores == nil {
		c.Cores = make(map[string]*CoreConfig)
	}
	c.Cores[core] = &CoreConfig{Type: typ}
}

func (c *Config) RemoveCore(core string) {
	delete(c.Cores, core)
}

func (c *Config) GetEncoder(encoder string) *EncoderConfig {
	return c.Encoders[encoder]
}

func (c *Config) AddEncoder(encoder string, typ EncoderProvider) {
	if _, exist := c.Encoders[encoder]; exist {
		return
	}
	if c.Encoders == nil {
		c.Encoders = make(map[string]*EncoderConfig)
	}
	c.Encoders[encoder] = &EncoderConfig{Type: typ}
}

func (c *Config) RemoveEncoder(encoder string) {
	delete(c.Encoders, encoder)
}
