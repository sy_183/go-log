package manager

import (
	"regexp"
	"strings"
)

type Matcher interface {
	Match(s string, ss []string) bool
}

type MatchEntry struct {
	Matcher
	Pattern string
}

func NewMatcherEntry(pattern string) (MatchEntry, error) {
	if len(pattern) >= 2 && pattern[0] == '/' && pattern[len(pattern)-1] == '/' {
		matcher, err := NewRegexMatcher(pattern[1 : len(pattern)-1])
		if err != nil {
			return MatchEntry{}, err
		}
		return MatchEntry{Matcher: matcher}, nil
	}
	return MatchEntry{Matcher: NewSliceMatcher(pattern)}, nil
}

type SliceMatcher []string

func NewSliceMatcher(s string) SliceMatcher {
	if strings.IndexByte(s, '/') >= 0 {
		return strings.Split(s, "/")
	}
	return strings.Split(s, "")
}

func (m SliceMatcher) Match(s string, ss []string) bool {
	ss = NewSliceMatcher(s)
	for i := range ss {
		if ss[i] != m[i] {
			return false
		}
	}
	return true
}

type RegexMatcher regexp.Regexp

func NewRegexMatcher(pattern string) (*RegexMatcher, error) {
	regex, err := regexp.Compile(pattern)
	if err != nil {
		return nil, err
	}
	return (*RegexMatcher)(regex), nil
}

func (m *RegexMatcher) Match(s string, ss []string) bool {
	return (*regexp.Regexp)(m).MatchString(s)
}
