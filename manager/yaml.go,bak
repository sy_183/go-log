package manager

import "gopkg.in/yaml.v3"

type YAMLHandler func(key string, value *yaml.Node) (ok bool, err error)

type yamlMapEntry struct {
	key   string
	value *yaml.Node
}

type YAMLMapUnmarshaler struct {
	value    *yaml.Node
	notFirst bool
	entries  []yamlMapEntry
}

func (u *YAMLMapUnmarshaler) unmarshalKeyValue(key *yaml.Node, value *yaml.Node, handler YAMLHandler) error {
	for value.Kind == yaml.AliasNode {
		value = value.Alias
		if key.Tag == "!!merge" {
			if value.Kind == yaml.MappingNode {
				return u.unmarshalMap(value, handler)
			}
			return nil
		}
	}
	if key.Tag == "!!str" {
		if ok, err := handler(key.Value, value); err != nil {
			return err
		} else if !ok {
			u.entries = append(u.entries, yamlMapEntry{key: key.Value, value: value})
		}
	}
	return nil
}

func (u *YAMLMapUnmarshaler) unmarshalMap(value *yaml.Node, handler YAMLHandler) error {
	for i := 0; i < len(value.Content)-1; i += 2 {
		key := value.Content[i]
		if key.Kind == yaml.ScalarNode {
			if err := u.unmarshalKeyValue(key, value.Content[i+1], handler); err != nil {
				return err
			}
		}
	}
	return nil
}

func (u *YAMLMapUnmarshaler) SetValue(value *yaml.Node) {
	u.value = value
	u.notFirst = false
	u.entries = u.entries[:0]
}

func (u *YAMLMapUnmarshaler) UnmarshalYAML(handler YAMLHandler) error {
	if u.notFirst {
		var entries []yamlMapEntry
		for _, entry := range u.entries {
			if ok, err := handler(entry.key, entry.value); err != nil {
				return err
			} else if !ok {
				entries = append(entries, entry)
			}
		}
		u.entries = entries
		return nil
	}
	// first parse
	value := u.value
	for value.Kind == yaml.AliasNode {
		value = value.Alias
	}
	if value.Kind == yaml.MappingNode {
		if err := u.unmarshalMap(value, handler); err != nil {
			u.entries = u.entries[:0]
			return err
		}
		u.notFirst = true
	}
	return nil
}
