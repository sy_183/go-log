package log

import (
	"gitee.com/csstx2020/cvds2020/go-common/option"
)

type Option = option.Setter[*Logger]

func WithCore(core Core) Option {
	if core == nil {
		core = NopCore
	}
	return option.Set[*Logger](func(target *Logger) {
		target.setCore(core)
	})
}

var (
	WithNopCore     = WithCore(NopCore)
	WithNopSinkCore = WithCore(NopSinkCore)
	WithStdoutCore  = WithCore(StdoutCore)
	WithStderrCore  = WithCore(StderrCore)
)

func WithSinkCore(sink Sink, options ...CoreOption) Option {
	return WithCore(NewSinkCore(sink, options...))
}

func WithAsyncSinkCore(sink Sink, options ...CoreOption) Option {
	return WithCore(NewAsyncSinkCore(sink, options...))
}

func WithFileCore(path string, options ...CoreOption) Option {
	return WithCore(NewFileCore(path, options...))
}

func WithAsyncFileCore(path string, options ...CoreOption) Option {
	return WithCore(NewAsyncFileCore(path, options...))
}

func WithEncoder(encoder Encoder) Option {
	return option.Set[*Logger](func(target *Logger) {
		target.setEncoder(encoder)
	})
}

func WithJsonEncoder(options ...EncoderOption) Option {
	return WithEncoder(NewJsonEncoder(options...))
}

func WithConsoleEncoder(options ...EncoderOption) Option {
	return WithEncoder(NewConsoleEncoder(options...))
}

func WithClock(clock Clock) Option {
	if clock == nil {
		clock = DefaultClock
	}
	return option.Set[*Logger](func(target *Logger) {
		target.setClock(clock)
	})
}

func WithErrLogger(logger Logger) Option {
	return option.Set[*Logger](func(target *Logger) {
		target.setErrLogger(logger)
	})
}

func WithDevelopment(development bool) Option {
	return option.Set[*Logger](func(target *Logger) {
		target.setDevelopment(development)
	})
}

func WithIgnoreError(ignore bool) Option {
	return option.Set[*Logger](func(target *Logger) {
		target.setIgnoreError(ignore)
	})
}

func WithAddCaller(addCaller LevelEnabler) Option {
	if addCaller == nil {
		addCaller = DefaultAddCaller
	}
	return option.Set[*Logger](func(target *Logger) {
		target.setAddCaller(addCaller)
	})
}

var (
	WithAlwaysEnableCaller  = WithAddCaller(LevelAlwaysEnable)
	WithAlwaysDisableCaller = WithAddCaller(LevelAlwaysDisable)
	WithTraceCaller         = WithAddCaller(TraceLevel)
	WithDebugCaller         = WithAddCaller(DebugLevel)
	WithInfoCaller          = WithAddCaller(InfoLevel)
	WithWarnCaller          = WithAddCaller(WarnLevel)
	WithErrorCaller         = WithAddCaller(ErrorLevel)
	WithDPanicCaller        = WithAddCaller(DPanicLevel)
	WithPanicCaller         = WithAddCaller(PanicLevel)
	WithFatalCaller         = WithAddCaller(FatalLevel)
	WithDefaultAddCaller    = WithAddCaller(DefaultAddCaller)
)

func WithAddStack(addStack LevelEnabler) Option {
	if addStack == nil {
		addStack = DefaultAddStack
	}
	return option.Set[*Logger](func(target *Logger) {
		target.setAddStack(addStack)
	})
}

var (
	WithAlwaysEnableStack  = WithAddStack(LevelAlwaysEnable)
	WithAlwaysDisableStack = WithAddStack(LevelAlwaysDisable)
	WithTraceStack         = WithAddStack(TraceLevel)
	WithDebugStack         = WithAddStack(DebugLevel)
	WithInfoStack          = WithAddStack(InfoLevel)
	WithWarnStack          = WithAddStack(WarnLevel)
	WithErrorStack         = WithAddStack(ErrorLevel)
	WithDPanicStack        = WithAddStack(DPanicLevel)
	WithPanicStack         = WithAddStack(PanicLevel)
	WithFatalStack         = WithAddStack(FatalLevel)
	WithDefaultAddStack    = WithAddStack(DefaultAddStack)
)

func WithName(name string) Option {
	return option.Set[*Logger](func(target *Logger) {
		target.setName(name)
	})
}

func WithLevel(level LevelEnabler) Option {
	if level == nil {
		level = DefaultLevel
	}
	return option.Set[*Logger](func(target *Logger) {
		target.setLevel(level)
	})
}

var (
	WithEnableAllLevel  = WithLevel(LevelAlwaysEnable)
	WithDisableAllLevel = WithLevel(LevelAlwaysDisable)
	WithTrace           = WithLevel(TraceLevel)
	WithDebug           = WithLevel(DebugLevel)
	WithInfo            = WithLevel(InfoLevel)
	WithWarn            = WithLevel(WarnLevel)
	WithError           = WithLevel(ErrorLevel)
	WithDPanic          = WithLevel(DPanicLevel)
	WithPanic           = WithLevel(PanicLevel)
	WithFatal           = WithLevel(FatalLevel)
	WithDefaultLevel    = WithLevel(DefaultLevel)
)

func WithFields(append bool, fields ...Field) Option {
	return option.Set[*Logger](func(target *Logger) {
		if append {
			target.appendFields(fields...)
		} else {
			target.setFields(fields...)
		}
	})
}

func WithCallerSkip(skip int) Option {
	return option.Set[*Logger](func(target *Logger) {
		target.callerSkip = skip
	})
}
