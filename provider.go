package log

type Provider interface {
	Logger() Logger
}
