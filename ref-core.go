package log

import "gitee.com/csstx2020/cvds2020/go-common/pool"

type RefCore struct {
	Core
	*pool.AtomicRef
}

func NewRefCore(core Core) RefCore {
	return RefCore{
		Core:      core,
		AtomicRef: new(pool.AtomicRef),
	}
}

func (c RefCore) Use() RefCore {
	c.AddRef()
	return c
}

func (c RefCore) Close() error {
	if c.Release() {
		return c.Core.Close()
	}
	return nil
}
