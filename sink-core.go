package log

import (
	"gitee.com/csstx2020/cvds2020/go-common/chans"
	"gitee.com/csstx2020/cvds2020/go-common/unit"
	"gitee.com/csstx2020/cvds2020/go-log/internal/bufferpool"
	"io"
	"os"
	"reflect"
	"sync"
	"sync/atomic"
	"time"
)

type WriteSyncer interface {
	io.Writer
	Sync() error
}

type Sink interface {
	WriteSyncer
	Close() error
}

type nopSink struct{}

func (s nopSink) Write(p []byte) (n int, err error) { return 0, nil }
func (s nopSink) Sync() error                       { return nil }
func (s nopSink) Close() error                      { return nil }

type writerSink struct{ io.Writer }

func WriterSink(writer io.Writer) Sink { return writerSink{writer} }
func (s writerSink) Sync() error       { return nil }
func (s writerSink) Close() error      { return nil }

type writeSyncerSink struct{ WriteSyncer }

func WriterSyncerSink(writer WriteSyncer) Sink { return writeSyncerSink{writer} }
func (s writeSyncerSink) Close() error         { return nil }

type writerCloserSink struct{ io.WriteCloser }

func WriterCloserSink(writer io.WriteCloser) Sink { return writerCloserSink{writer} }
func (s writerCloserSink) Sync() error            { return nil }

type AtomicSink struct {
	sink atomic.Pointer[Sink]
}

func NewAtomicSink(sink Sink) *AtomicSink {
	s := new(AtomicSink)
	s.SetSink(sink)
	return s
}

func (s *AtomicSink) Sink() Sink {
	if sink := s.sink.Load(); sink != nil {
		return *sink
	}
	return nil
}

func (s *AtomicSink) SetSink(sink Sink) {
	s.sink.Store(&sink)
}

func (s *AtomicSink) Write(p []byte) (n int, err error) {
	if sink := s.Sink(); sink != nil {
		return sink.Write(p)
	}
	return 0, nil
}

func (s *AtomicSink) Sync() error {
	if sink := s.Sink(); sink != nil {
		return sink.Sync()
	}
	return nil
}

func (s *AtomicSink) Close() error {
	if sink := s.Sink(); sink != nil {
		return sink.Close()
	}
	return nil
}

var (
	NopSink = Sink(nopSink{})
	Stdout  = WriterSyncerSink(os.Stdout)
	Stderr  = WriterSyncerSink(os.Stderr)
)

type SinkCore struct {
	core         Core
	sink         Sink
	errorHandler CoreErrorHandler
}

func newSinkCore(sink Sink) SinkCore {
	if sink == nil {
		sink = NopSink
	}
	return SinkCore{
		sink:         sink,
		errorHandler: DefaultCoreErrorHandler,
	}
}

func NewSinkCore(sink Sink, options ...CoreOption) *SinkCore {
	c := newSinkCore(sink)
	c.init(&c, options...)
	return &c
}

func ProvideSinkCore(options ...CoreOption) Core {
	return NewSinkCore(nil, options...)
}

func (c *SinkCore) init(core Core, options ...CoreOption) {
	c.core = core
	for _, option := range options {
		option.Apply(core)
	}
}

func (c *SinkCore) setSink(sink Sink)                        { c.sink = sink }
func (c *SinkCore) setErrorHandler(handler CoreErrorHandler) { c.errorHandler = handler }

func (c *SinkCore) Open() error { return nil }

func (c *SinkCore) Write(logger Logger, entry *Entry, fields []Field, b any) error {
	if buffer, _ := b.(*bufferpool.Buffer); buffer != nil {
		defer buffer.Free()
		if _, err := c.sink.Write(buffer.Bytes()); err != nil {
			if c.errorHandler != nil {
				c.errorHandler(c, logger, err)
			}
			return err
		}
		if entry.Level.Level() > ErrorLevel {
			c.Sync()
		}
	}
	return nil
}

func (c SinkCore) Sync() error {
	return c.sink.Sync()
}

func (c SinkCore) Close() error {
	return c.sink.Close()
}

var (
	NopSinkCore = NewSinkCore(NopSink)
	StdoutCore  = NewSinkCore(Stdout)
	StderrCore  = NewSinkCore(Stderr)
)

type (
	AsyncDropHandler func(core Core, logger Logger, dropped uint64)
)

func LogAsyncStatisticsDropHandler(core Core, logger Logger, dropped uint64) {
	if !logger.IsNop() {
		logger.Warn("异步日志输出器丢弃日志信息", Stringer("输出器类型", reflect.TypeOf(core)), Uint64("丢弃日志数量", dropped))
	}
}

func ThresholdAsyncDropHandler(threshold int, handler AsyncDropHandler) AsyncDropHandler {
	if threshold <= 0 {
		threshold = 100
	}
	if handler == nil {
		handler = LogAsyncStatisticsDropHandler
	}
	return func(core Core, logger Logger, dropped uint64) {
		if dropped%uint64(threshold) == 0 {
			handler(core, logger, dropped)
		}
	}
}

func IntervalAsyncDropHandler(interval time.Duration, handler AsyncDropHandler) AsyncDropHandler {
	if interval <= 0 {
		interval = time.Second
	}
	if handler == nil {
		handler = LogAsyncStatisticsDropHandler
	}
	var last time.Time
	return func(core Core, logger Logger, dropped uint64) {
		now := time.Now()
		if last == (time.Time{}) {
			last = now
		}
		if now.Sub(last) > interval {
			handler(core, logger, dropped)
			last = now
		}
	}
}

type asyncBuffer struct {
	*bufferpool.Buffer
	syncWg *sync.WaitGroup
	logger Logger
}

type AsyncSinkCore struct {
	SinkCore
	queueSize int
	queue     chan asyncBuffer

	bufferSize int
	buffer     *bufferpool.Buffer

	dropHandler AsyncDropHandler
	dropped     atomic.Uint64

	logger  Logger
	closeWg sync.WaitGroup
}

func newAsyncSinkCore(sink Sink) AsyncSinkCore {
	return AsyncSinkCore{
		SinkCore:    newSinkCore(sink),
		queueSize:   1024,
		bufferSize:  unit.MeBiByte,
		dropHandler: IntervalAsyncDropHandler(0, nil),
	}
}

func NewAsyncSinkCore(sink Sink, options ...CoreOption) *AsyncSinkCore {
	c := newAsyncSinkCore(sink)
	c.init(&c, options...)
	return &c
}

func ProviderAsyncSinkCore(options ...CoreOption) Core {
	return NewAsyncSinkCore(nil, options...)
}

func (c *AsyncSinkCore) init(core Core, options ...CoreOption) {
	c.SinkCore.init(core, options...)
	c.queue = make(chan asyncBuffer, c.queueSize)
	c.buffer = bufferpool.NewBuffer(c.bufferSize)
	c.closeWg.Add(1)
	go c.run()
}

func (c *AsyncSinkCore) setQueueSize(size int)                   { c.queueSize = size }
func (c *AsyncSinkCore) setBufferSize(size int)                  { c.bufferSize = size }
func (c *AsyncSinkCore) setDropHandler(handler AsyncDropHandler) { c.dropHandler = handler }

// write 将将指定buffer中数据写入到缓存中
func (c *AsyncSinkCore) write(buffer *bufferpool.Buffer) {
	defer buffer.Free()
	c.buffer.Write(buffer.Bytes())
}

// flush 将缓存中的数据写入到IO流中
func (c *AsyncSinkCore) flush() {
	if c.buffer.Len() > 0 {
		if _, err := c.sink.Write(c.buffer.Bytes()); err != nil {
			c.errorHandler(c, c.logger, err)
		}
		c.buffer.Reset()
	}
}

// direct 直接将指定buffer中数据写入到IO流中
func (c *AsyncSinkCore) direct(buffer *bufferpool.Buffer) *bufferpool.Buffer {
	if buffer == nil {
		return nil
	}
	defer buffer.Free()
	if _, err := c.sink.Write(buffer.Bytes()); err != nil {
		c.errorHandler(c, c.logger, err)
	}
	return nil
}

func (c *AsyncSinkCore) sync(wg *sync.WaitGroup) {
	defer wg.Done()
	if err := c.sink.Sync(); err != nil {
		c.errorHandler(c, c.logger, err)
	}
}

func (c *AsyncSinkCore) handleSub(buffer *bufferpool.Buffer, syncWg *sync.WaitGroup) {
	if buffer != nil {
		if buffer.Len() > c.bufferSize {
			c.flush()
			c.direct(buffer)
		} else if buffer.Len()+c.buffer.Len() > c.bufferSize {
			c.flush()
			c.write(buffer)
		} else {
			c.write(buffer)
		}
	} else if syncWg != nil {
		c.flush()
		c.sync(syncWg)
	}
}

func (c *AsyncSinkCore) handle(buffer *bufferpool.Buffer, syncWg *sync.WaitGroup) bool {
	if buffer != nil {
		if buffer.Len() > c.bufferSize {
			c.direct(buffer)
			return true
		}
		c.write(buffer)
		for {
			select {
			case buf, ok := <-c.queue:
				if !ok {
					c.flush()
					return false
				}
				c.logger = buf.logger
				c.handleSub(buf.Buffer, buf.syncWg)
			default:
				c.flush()
				return true
			}
		}
	} else if syncWg != nil {
		c.sync(syncWg)
	}
	return true
}

func (c *AsyncSinkCore) run() {
	for buf := range c.queue {
		c.logger = buf.logger
		c.handle(buf.Buffer, buf.syncWg)
	}
	c.closeWg.Done()
}

func recoverToError(e any, err error) error {
	if e != nil {
		if err, is := e.(error); is {
			return err
		}
		panic(e)
	}
	return err
}

func (c *AsyncSinkCore) queueTryPush(buf asyncBuffer) (ok bool, err error) {
	defer func() { err = recoverToError(recover(), err) }()
	return chans.TryPush(c.queue, buf), nil
}

func (c *AsyncSinkCore) queuePush(buf asyncBuffer) (err error) {
	defer func() { err = recoverToError(recover(), err) }()
	c.queue <- buf
	return nil
}

func (c *AsyncSinkCore) queueClose() (err error) {
	defer func() { err = recoverToError(recover(), err) }()
	close(c.queue)
	return nil
}

func (c *AsyncSinkCore) Dropped() uint64 {
	return c.dropped.Load()
}

func (c *AsyncSinkCore) Write(logger Logger, entry *Entry, fields []Field, b any) error {
	if buffer, _ := b.(*bufferpool.Buffer); buffer != nil {
		if ok, err := c.queueTryPush(asyncBuffer{Buffer: buffer, logger: logger}); err != nil {
			return err
		} else if !ok {
			dropped := c.dropped.Add(1)
			if c.dropHandler != nil {
				c.dropHandler(c.core, logger, dropped)
			}
		}
	}
	return nil
}

func (c *AsyncSinkCore) Sync() error {
	wg := new(sync.WaitGroup)
	if err := c.queuePush(asyncBuffer{syncWg: wg}); err != nil {
		return err
	}
	wg.Wait()
	return nil
}

func (c *AsyncSinkCore) closeQueueWait() bool {
	if err := c.queueClose(); err != nil {
		return false
	}
	c.closeWg.Wait()
	return true
}

func (c *AsyncSinkCore) Close() error {
	if c.closeQueueWait() {
		return c.sink.Close()
	}
	return nil
}
