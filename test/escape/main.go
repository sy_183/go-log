package main

import (
	"fmt"
	"gitee.com/csstx2020/cvds2020/go-common/unit"
	"gitee.com/csstx2020/cvds2020/go-log"
	"os"
	"time"
)

func init() {
	log.GlobalWithEncoderOptions(log.WithCapitalColorLevelFormatter)
}

func main() {
	logger := log.New(
		log.WithDebug,
		log.WithConsoleEncoder(log.WithCapitalColorLevelFormatter, log.WithJsonDefaultIndent),
		log.WithAsyncFileCore("test.log", log.WithFileFlag(os.O_SYNC|os.O_TRUNC), log.WithQueueSize(4096), log.WithBufferSize(unit.MeBiByte)),
		log.WithNopSinkCore,
		log.WithName("test"),
	)
	if err := logger.Core().Open(); err != nil {
		log.Fatalf("open core error: {}", err)
	}
	go func() {
		count := 1
		fc, _ := logger.Core().(*log.AsyncFileCore)
		if fc != nil {
			for range time.Tick(time.Second) {
				logger.Core().(*log.AsyncFileCore).SetPath(fmt.Sprintf("test-%d.log", count))
				logger.Core().Open()
				count++
			}
		}
	}()
	al := log.NewAtomic(logger)
	_ = al
	start := time.Now()
	num := 10000000
	for i := 0; i < num; i++ {
		//logger.Debugf("test logger: seq={}, strings key1={}, bool key1={}", i, "string value 1", true)
		al.Debug("test logger",
			log.Int("seq", i),
			log.String("string key1", "string value 1"),
			log.Bool("bool key1", true))
		//time.Sleep(time.Second)
	}
	logger.Core().Close()
	du := time.Now().Sub(start)
	var dropped uint64
	if dc, _ := logger.Core().(interface{ Dropped() uint64 }); dc != nil {
		dropped = dc.Dropped()
	}
	log.Info("log done",
		log.Int("num", num),
		log.Float64("speed", float64(num)/(float64(du)/float64(time.Second))),
		log.Uint64("dropped", dropped),
		log.Duration("duration", du),
	)
}
