package log

func clone[O any](o *O) *O {
	n := *o
	return &n
}
