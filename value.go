package log

import (
	"fmt"
	sliceUnsafe "gitee.com/csstx2020/cvds2020/go-common/slice/unsafe"
	stringUnsafe "gitee.com/csstx2020/cvds2020/go-common/strings/unsafe"
	"math"
	"time"
)

type ValueType uint8

const (
	// UnknownType 默认的值类型
	UnknownType ValueType = iota
	// SkipType 代表忽略此类型的值
	SkipType
	// NilType 代表 nil 类型的值
	NilType
	// BinaryType 代表 []byte 类型的值
	BinaryType
	// BoolType 代表 bool 类型的值
	BoolType
	// Complex128Type complex128 类型
	Complex128Type
	// Complex64Type 代表 complex64 类型的值
	Complex64Type
	// Float64Type 代表 float64 类型的值
	Float64Type
	// Float32Type 代表 float32 类型的值
	Float32Type
	// Int64Type 代表 int64 类型的值
	Int64Type
	// Int32Type 代表 int32 类型的值
	Int32Type
	// Int16Type 代表 int16 类型的值
	Int16Type
	// Int8Type 代表 int8 类型的值
	Int8Type
	// StringType 代表 string 类型的值
	StringType
	// Uint64Type 代表 uint64 类型的值
	Uint64Type
	// Uint32Type 代表 uint32 类型的值
	Uint32Type
	// Uint16Type 代表 uint16 类型的值
	Uint16Type
	// Uint8Type 代表 uint8 类型的值
	Uint8Type
	// UintptrType 代表 uintptr 类型的值
	UintptrType
	// TimeType 代表 time.Time 类型的值，存储为 UnixNano 格式
	TimeType
	// TimeFullType 代表 time.Time 类型的值
	TimeFullType
	// DurationType 代表 time.Duration 类型的值
	DurationType
	// StringerType 代表 fmt.Stringer 类型的值
	StringerType
	// FormatterType 代表 Formatter 类型的值
	FormatterType
	// ErrorType 代表 error 类型的值
	ErrorType
	// ObjectType 代表 any 类型的值
	ObjectType
	// SliceType 代表 slice 类型的值
	SliceType
	// MapType 代表 map 类型的值
	MapType
	// ValuesType 代表 []Value 类型的值
	ValuesType
	// FieldsType 代表 []Field 类型的值
	FieldsType
	MaxTypes
)

type InvalidValueTypeError struct {
	Type ValueType
}

func (e InvalidValueTypeError) Error() string {
	return fmt.Sprintf("无效的值类型(%d)", e.Type)
}

type Value struct {
	Type  ValueType
	int64 int64
	raw   string
	obj   any
}

func int64V(typ ValueType, val int64) Value { return Value{Type: typ, int64: val} }
func rawV(typ ValueType, raw string) Value  { return Value{Type: typ, raw: raw} }
func objV(typ ValueType, obj any) Value {
	if obj == nil {
		return NilV
	}
	return Value{Type: typ, obj: obj}
}

func (v *Value) time() time.Time {
	if v.obj != nil {
		return time.Unix(0, v.int64).In(v.obj.(*time.Location))
	} else {
		return time.Unix(0, v.int64)
	}
}

func (v *Value) Binary() []byte            { return stringUnsafe.Bytes(v.raw) }
func (v *Value) Bool() bool                { return v.int64 != 0 }
func (v *Value) Complex128() complex128    { return v.obj.(complex128) }
func (v *Value) Complex64() complex64      { return v.obj.(complex64) }
func (v *Value) Float64() float64          { return math.Float64frombits(uint64(v.int64)) }
func (v *Value) Float32() float32          { return math.Float32frombits(uint32(v.int64)) }
func (v *Value) Int() int                  { return int(v.int64) }
func (v *Value) Int64() int64              { return v.int64 }
func (v *Value) Int32() int32              { return int32(v.int64) }
func (v *Value) Int16() int16              { return int16(v.int64) }
func (v *Value) Int8() int8                { return int8(v.int64) }
func (v *Value) String() string            { return v.raw }
func (v *Value) Uint() uint                { return uint(v.int64) }
func (v *Value) Uint64() uint64            { return uint64(v.int64) }
func (v *Value) Uint32() uint32            { return uint32(v.int64) }
func (v *Value) Uint16() uint16            { return uint16(v.int64) }
func (v *Value) Uint8() uint8              { return uint8(v.int64) }
func (v *Value) Uintptr() uintptr          { return uintptr(v.int64) }
func (v *Value) Time() time.Time           { return v.time() }
func (v *Value) TimeFull() time.Time       { return v.obj.(time.Time) }
func (v *Value) Duration() time.Duration   { return time.Duration(v.int64) }
func (v *Value) Stringer() fmt.Stringer    { return v.obj.(fmt.Stringer) }
func (v *Value) Formatter() ValueFormatter { return v.obj.(ValueFormatter) }
func (v *Value) Error() error              { return v.obj.(error) }
func (v *Value) Object() any               { return v.obj }
func (v *Value) Values() []Value           { return v.obj.([]Value) }
func (v *Value) Fields() []Field           { return v.obj.([]Field) }

var (
	minTimeInt64 = time.Unix(0, math.MinInt64)
	maxTimeInt64 = time.Unix(0, math.MaxInt64)
)

func boolToInt64(b bool) int64 {
	if b {
		return 1
	} else {
		return 0
	}
}

func timeV(val time.Time) Value {
	if val.Before(minTimeInt64) || val.After(maxTimeInt64) {
		return Value{Type: TimeFullType, obj: val}
	}
	return Value{Type: TimeType, int64: val.UnixNano(), obj: val.Location()}
}

var (
	NilV  = Value{Type: NilType}
	SkipV = Value{Type: SkipType}
)

func BinaryV(val []byte) Value                    { return rawV(BinaryType, sliceUnsafe.String(val)) }
func BoolV(val bool) Value                        { return int64V(BoolType, boolToInt64(val)) }
func Complex128V(val complex128) Value            { return objV(Complex128Type, val) }
func Complex64V(val complex64) Value              { return objV(Complex64Type, val) }
func Float64V(val float64) Value                  { return int64V(Float64Type, int64(math.Float64bits(val))) }
func Float32V(val float32) Value                  { return int64V(Float32Type, int64(math.Float32bits(val))) }
func IntV(val int) Value                          { return Int64V(int64(val)) }
func Int64V(val int64) Value                      { return int64V(Int64Type, val) }
func Int32V(val int32) Value                      { return int64V(Int32Type, int64(val)) }
func Int16V(val int16) Value                      { return int64V(Int16Type, int64(val)) }
func Int8V(val int8) Value                        { return int64V(Int8Type, int64(val)) }
func StringV(val string) Value                    { return rawV(StringType, val) }
func UintV(val uint) Value                        { return Uint64V(uint64(val)) }
func Uint64V(val uint64) Value                    { return int64V(Uint64Type, int64(val)) }
func Uint32V(val uint32) Value                    { return int64V(Uint32Type, int64(val)) }
func Uint16V(val uint16) Value                    { return int64V(Uint16Type, int64(val)) }
func Uint8V(val uint8) Value                      { return int64V(Uint8Type, int64(val)) }
func UintptrV(val uintptr) Value                  { return int64V(UintptrType, int64(val)) }
func TimeV(val time.Time) Value                   { return timeV(val) }
func DurationV(val time.Duration) Value           { return int64V(DurationType, int64(val)) }
func StringerV(val fmt.Stringer) Value            { return objV(StringerType, val) }
func FormatterV(val ValueFormatter) Value         { return objV(FormatterType, val) }
func ErrorV(val error) Value                      { return objV(ErrorType, val) }
func ObjectV(val any) Value                       { return objV(ObjectType, val) }
func SliceV[V any](val []V) Value                 { return objV(SliceType, val) }
func MapV[K comparable, V any](val map[K]V) Value { return objV(MapType, val) }
func ValuesV(values ...Value) Value               { return objV(ValuesType, values) }
func FieldsV(fields ...Field) Value               { return objV(FieldsType, fields) }

func AnyV(val any) Value {
	switch val := val.(type) {
	case nil:
		return NilV
	case []byte:
		return BinaryV(val)
	case bool:
		return BoolV(val)
	case complex128:
		return Complex128V(val)
	case complex64:
		return Complex64V(val)
	case float64:
		return Float64V(val)
	case float32:
		return Float32V(val)
	case int:
		return IntV(val)
	case int64:
		return Int64V(val)
	case int32:
		return Int32V(val)
	case int16:
		return Int16V(val)
	case int8:
		return Int8V(val)
	case string:
		return StringV(val)
	case uint:
		return UintV(val)
	case uint64:
		return Uint64V(val)
	case uint32:
		return Uint32V(val)
	case uint16:
		return Uint16V(val)
	case uint8:
		return Uint8V(val)
	case uintptr:
		return UintptrV(val)
	case time.Time:
		return TimeV(val)
	case time.Duration:
		return DurationV(val)
	case fmt.Stringer:
		return StringerV(val)
	case ValueFormatter:
		return FormatterV(val)
	case error:
		return ErrorV(val)
	case []Value:
		return ValuesV(val...)
	case []Field:
		return FieldsV(val...)
	default:
		return ObjectV(val)
	}
}

func BinaryPV(val *[]byte) Value {
	if val == nil {
		return NilV
	}
	return BinaryV(*val)
}

func BoolPV(val *bool) Value {
	if val == nil {
		return NilV
	}
	return BoolV(*val)
}

func Float32PV(val *float32) Value {
	if val == nil {
		return NilV
	}
	return Float32V(*val)
}

func Complex128PV(val *complex128) Value {
	if val == nil {
		return NilV
	}
	return Complex128V(*val)
}

func Complex64PV(val *complex64) Value {
	if val == nil {
		return NilV
	}
	return Complex64V(*val)
}

func Float64PV(val *float64) Value {
	if val == nil {
		return NilV
	}
	return Float64V(*val)
}

func IntPV(val *int) Value {
	if val == nil {
		return NilV
	}
	return IntV(*val)
}

func Int64PV(val *int64) Value {
	if val == nil {
		return NilV
	}
	return Int64V(*val)
}

func Int32PV(val *int32) Value {
	if val == nil {
		return NilV
	}
	return Int32V(*val)
}

func Int16PV(val *int16) Value {
	if val == nil {
		return NilV
	}
	return Int16V(*val)
}

func Int8PV(val *int8) Value {
	if val == nil {
		return NilV
	}
	return Int8V(*val)
}

func StringPV(val *string) Value {
	if val == nil {
		return NilV
	}
	return StringV(*val)
}

func UintPV(val *uint) Value {
	if val == nil {
		return NilV
	}
	return UintV(*val)
}

func Uint64PV(val *uint64) Value {
	if val == nil {
		return NilV
	}
	return Uint64V(*val)
}

func Uint32PV(val *uint32) Value {
	if val == nil {
		return NilV
	}
	return Uint32V(*val)
}

func Uint16PV(val *uint16) Value {
	if val == nil {
		return NilV
	}
	return Uint16V(*val)
}

func Uint8PV(val *uint8) Value {
	if val == nil {
		return NilV
	}
	return Uint8V(*val)
}

func UintptrPV(val *uintptr) Value {
	if val == nil {
		return NilV
	}
	return UintptrV(*val)
}

func TimePV(val *time.Time) Value {
	if val == nil {
		return NilV
	}
	return TimeV(*val)
}

func DurationPV(val *time.Duration) Value {
	if val == nil {
		return NilV
	}
	return DurationV(*val)
}
