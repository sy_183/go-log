package log

import (
	"io"
	"time"
)

type Writer interface {
	AppendByte(v byte)
	AppendString(s string)
	AppendInt(i int64)
	AppendTime(t time.Time, layout string)
	AppendUint(i uint64)
	AppendBool(v bool)
	AppendFloat(f float64, bitSize int)
	io.Writer
	io.ByteWriter
	io.StringWriter
}
