package log

import (
	"gopkg.in/yaml.v3"
	"strings"
	"testing"
)

func TestYaml(t *testing.T) {
	sb := strings.Builder{}
	enc := yaml.NewEncoder(&sb)
	if err := enc.Encode(map[string]string{
		//"hello world": sgr.WrapCSI("hello\nworld\n", sgr.FgRedCSI),
		"hello world": "hello\nworld\n",
	}); err != nil {
		panic(err)
	}
	print(sb.String())
}
